import axios from 'axios';

import storeData from '@/store/data';

axios.defaults.baseURL = API_URL;
axios.defaults.headers['Content-Type'] = 'application/x-www-form-urlencoded';

export default function useHttp() {

    const send = async (url, data = null, loading = true) => {

        let connectId = localStore('X-CONNECT-ID');
        if(!connectId) connectId = 0;
        axios.defaults.headers['X-CONNECT-ID'] = connectId;

        try {

            let response = {};
            if (data) response = await axios.post(url, data)
            else      response = await axios.get(url)

            storeData.error = null;

            if(response?.data?.error) {
                storeData.error = response.data.error;
                storeData.error['url'] = url;
                storeData.error['data'] = data;
                storeData.error['headers'] = axios.defaults.headers;
                console.log('HTTP-SEND-ERROR:')
                console.log(storeData.error)
            }

            return response.data

        } catch (error) {
            console.error(error.toJSON())
        }
    }

    const store = (field, data = null) => {
        if(!data) {
            let result = null;
            if (storeData[field])
                result = storeData[field]
            return result
        }

        if (storeData[field]) {
            storeData[field] = data
        }
    }

    const preloader = (value = false) => {
        storeData.preloader = value;
    }

    const localStore = (key, data = null) => {
        if(!data) {
            let value = localStorage.getItem(key)
            return (value) ? value : null;
        }
        return localStorage.setItem(key, data);
    }

    const removeLocalStore = (key) => {
        localStorage.removeItem(key);
    }

    return {
        send,
        preloader,
        storeData,
        store,
        localStore,
        removeLocalStore,
    }
}