import {reactive} from "vue";

const storeData = reactive({

    action     : '',
    preloader  : false,
    error      : null,
    connect_id : null,
    dbname     : null,
    tableName  : null,

    connections: [],
    databases: [],
    users: [],
    tables: [],

    table: {},
    dbItem: {},
    fields: {},
    connect: {},
    zIndex: 10,

})

export default storeData;