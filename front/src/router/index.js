import { createRouter, createWebHistory } from 'vue-router'
import Home from '@/pages/Home.vue'
import DbManager from '@/pages/DbManager.vue'
import DbMigrate from '@/pages/DbMigrate.vue'
import DbTablesData from '@/pages/DbTablesData.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },

  {
    path: '/db-manager/:object/:id',
    name: 'db-manager-item-object',
    component: DbManager
  },

  {
    path: '/db-manager',
    name: 'db-manager',
    component: DbManager
  },

  {
    path: '/db-migrate',
    name: 'db-migrate',
    component: DbMigrate
  },

  {
    path: '/db-tables-data',
    name: 'db-tables-data',
    component: DbTablesData
  },

]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
  linkActiveClass: "active",
  // linkExactActiveClass: "active",
})

export default router
