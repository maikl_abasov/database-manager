
function useJquery() {

    // let selector = null;
    // let list = null;

    const selectedList = (selectorName = null) => {
        selectorName = (selectorName)? selectorName : selector;
        const list = document.querySelectorAll(selectorName);
        return list;
    }

    const $jquery = (selectorName) => {
        const list = document.querySelectorAll(selectorName);
        const on = (fn, event = 'click') => {
            list.forEach(elem => {
                elem.addEventListener(event, () => fn(elem))
            });
        }
        return  {
            on,
        }
    }

    const $toggle = (initSelector, destSelector) => {
         $jquery(initSelector).on((el) => {
             toggle(destSelector)
         }, 'click');
    }

    const $changeClass = (initSelector) => {
        const change = (firstClass, secondClass) => {
            $jquery(initSelector).on((el) => {
                changeClass(el, firstClass, secondClass)
            }, 'click');
        }
        return { change }
    }

    const $setActiveClass = (selector, activeClass) => {
         $jquery(selector).on(elem => {
             const list = document.querySelectorAll(selector);
             list.forEach(el => el.classList.remove(activeClass));
             elem.classList.add(activeClass)
         })
    }

    const on = (fn, event = 'click') => {
        const list = selectedList();
        list.forEach(elem =>
            elem.addEventListener(event, () => fn(elem))
        );
    }

    const toggle = (destSelector) => {
        const elem = document.querySelector(destSelector);
        let display = elem.style.display;
        display = (display == 'none' || display == '') ? 'block' : 'none';
        elem.style.display = display;
        return display;
    }

    const checkClass = (elem)  => {
        return (elem.classList.contains(primary)) ? true : false;
    }

    const changeClass = (elem, firstClass, secondClass) => {
        let remove = secondClass
        let add = firstClass;
        if (elem.classList.contains(firstClass)) {
            remove = firstClass
            add = secondClass;
        }
        elem.classList.remove(remove)
        elem.classList.add(add)
    }







    ////////////////////////////////////////

    const $click = (selector, fn) => {
        const list = document.querySelectorAll(selector);
        list.forEach(elem =>
            elem.addEventListener('click', () => fn(elem) )
        );
    }

    // const $toggle = (selector, nextSelector = null) => {
    //     let nextElem = null;
    //     const list = document.querySelectorAll(selector);
    //     const getNextElem = (elem) => {
    //         if(!nextSelector) return elem.nextElementSibling;
    //         let tag = selector + nextSelector;
    //         return document.querySelector(tag);
    //     }
    //
    //     const toggle = (el) => {
    //         let display = el.style.display;
    //         display = (display == 'none') ? 'block' : 'none';
    //         el.style.display = display
    //     }
    //
    //     list.forEach(elem => {
    //         nextElem = getNextElem(elem);
    //         nextElem.style.display = 'none';
    //         elem.addEventListener('click', () => {
    //             let next = getNextElem(elem);
    //             toggle(next);
    //         })
    //     });
    // }

    const $list = (selector, fn) => {
        const list = document.querySelectorAll(selector);
        list.forEach(elem => fn(elem) );
    }

    const $active = (selector, fnList, active) => {
        const list = document.querySelectorAll(selector);
        const listRender = () => {
            list.forEach(elem => fnList(elem));
        }

        list.forEach(elem =>
            elem.addEventListener('click', () => {
                listRender()
                active(elem);
            })
        );
    }

    const $activeClass = (selector, activeClass) => {
        const list = document.querySelectorAll(selector);
        const listRender = () => {
            list.forEach(el => el.classList.remove(activeClass));
        }

        list.forEach(elem =>
            elem.addEventListener('click', () => {
                listRender()
                elem.classList.add(activeClass)
            })
        );
    }

    const setClassItem = (selector, activeClass, index = 0) => {
        const list = document.querySelectorAll(selector);
        const item = list[index];
        if(!item) return false
        item.classList.add(activeClass)
    }

    const itemsRender = (items, fn) => {
        for(let i in items) {
            let elem = items[i];
            fn(elem)
        }
        return items;
    }

    return {
        $jquery,
        $toggle,
        $changeClass,
        $setActiveClass,

        $list,
        $active,
        $activeClass,
        $click,
        setClassItem,
        itemsRender,
    }
}

export default useJquery
