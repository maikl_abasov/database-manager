import { notify } from "@kyvg/vue3-notification";

import useHttp from "@/api/http";

const { send, storeData, localStore } = useHttp()

export default function useHandlers() {

    const notifyMessage = (text, type = 'success', title = '', duration = 5000) => {
        notify({title, text, type,  duration,});
    }

    const responseMessage = (status, okMessage = 'Успешное выполнение', errorMessage = 'Ошибка, неудачное выполнение', type = 'success') => {
        let message = okMessage;
        if(!status) {
            type = 'error';
            message = errorMessage;
        }
        notifyMessage(message, type);
    }

    const saveResponseHandle = (status, okMessage, errMessage) => {
        let message = errMessage;
        let type = 'error'
        if(status === 0) {
            message = okMessage;
            type = 'success'
        }
        notifyMessage(message, type)
    }

    const getConnections = async (connectId = null) => {
        let url = "/connection/list"
        const response = await send(url);
        storeData.connections = response;
        connectionsSetInit(connectId);
        return response;
    }

    const createConnect = async (data) => {
        let url = "/connection/create";
        const response = await send(url, data);
        return response;
    }

    const updateConnect = async (data, key) => {
        let url = "/connection/update/" + key;
        const response = await send(url, data);
        return response;
    }

    const deleteConnect = async ( key) => {
        let url = "/connection/delete/" + key;
        const response = await send(url);
        return response;
    }

    const checkConnect = async ( key) => {
        let url = "/connection/check-connect/" + key;
        const response = await send(url);
        return response;
    }

    const getDatabases = async (connectId = null, tableStatus = false) => {
        let url = "/db-manager/databases/list";
        const databases = await send(url);

        // if(tableStatus) {
        //   for(let i in databases) {
        //       let item = databases[i];
        //       getTables(item.name, true).then(tables => {
        //           databases[i]['tables'] = tables;
        //       })
        //   }
        // }

        storeData.databases = databases;

        return databases;
    }

    const createDatabase = async (dbName) => {
        let url = "/db-manager/get/create_database?dbname=" + dbName;
        const response = await send(url);
        getDatabases();
        return response;
    }

    const deleteDatabase = async (dbName) => {

        if(dbName == 'template1' || dbName == 'template0' || dbName == 'postgres') {
            alert('Нельзя удалять - ' + dbName);
            return false
        }

        let url = "/db-manager/get/delete_database?dbname=" + dbName;
        const response = await send(url);
        getDatabases();
        return response;
    }

    const copyDatabase = async (dbName, newDbName) => {
        let url = "/db-manager/get/copy_database?dbname=" + dbName + "&new_dbname=" + newDbName;
        const response = await send(url);
        getDatabases();
        return response;
    }

    const getUsers = async () => {
        let url = "/db-manager/get/users";
        const response = await send(url);
        storeData.users = response;
        return response;
    }

    const createUser = async (name, password) => {
        let url = "/db-manager/get/create_user?name=" + name + "&password=" + password;
        const response = await send(url);
        getUsers()
        return response;
    }

    const userPrivilegesChange = async (dbname, user, action) => {
        let url = "/db-manager/get/" +action+ "?dbname=" + dbname + "&user=" + user;
        const response = await send(url);
        return response;
    }

    const createTable = async (name, id) => {
        let url = "/db-manager/get/create_table?name=" + name + "&id=" + id ;
        const response = await send(url);
        getTables();
        return response;
    }

    const getTables = async (dbname = null, getStatus = false) => {
        if(!dbname)  dbname = storeData.dbname;
        let url = "/db-manager/tables/list/" + dbname ;
        const response = await send(url);
        if(!getStatus) storeData.tables = response;
        return response;
    }

    const dropTable = async (tableName) => {
        let url = "/db-manager/get/drop_table?name=" + tableName;
        const response = await send(url);
        getTables();
        return response;
    }

    const copyTable = async (oldTableName, newTableName) => {
        let url = "/db-manager/get/copy_table?old_name=" + oldTableName + "&new_name=" + newTableName;
        const response = await send(url);
        getTables();
        return response;
    }

    const renameTable = async (oldTableName, newTableName) => {
        let url = "/db-manager/get/rename_table?old_name=" + oldTableName + "&new_name=" + newTableName;
        const response = await send(url);
        getTables();
        return response;
    }

    const getTableFields = async (tableName, dbName) => {
        let url = "/db-manager/get/get_table_fields?name=" + tableName + "&db=" + dbName;
        const response = await send(url);
        return response;
    }

    const addField = async (data) => {
        let url = "/db-manager/post/add_field";
        const response = await send(url, data);
        return response;
    }

    const dropField = async (tableName, fname) => {
        let url = "/db-manager/get/drop_field?table_name=" + tableName + "&field_name=" + fname;
        const response = await send(url);
        return response;
    }

    const query = async (url, data = null, fn = null) => {
        let response = null;
        if(data) response = await send(url, data);
        else response = await send(url);
        if(fn) fn(response);
        return response;
    }

    const freeSqlMake = async (sql, type = 'query') => {
        let url = "/db-manager/free-sql-make";
        const data = { sql, type }
        const response = await send(url, data);
        return response;
    }

    const setActiveConnect = (connectId, connect) => {

        storeData.tables     = [];
        storeData.users      = [];

        storeData.dbname     = connect?.dbname;
        storeData.connect_id = connectId;
        storeData.connect    = connect;

        localStore('X-CONNECT-ID', connectId);
        getDatabases(connectId);   // получаем базы
        getUsers();                // получаем пользователей
        getTables(connect?.dbname); // получаем таблицы

        for(let key in storeData.connections) {
            let isActive = (key == connectId) ? true : false;
            storeData.connections[key]['is_active'] = isActive
        }
    }

    const reloadPage = () => {
        let connectId = localStore('X-CONNECT-ID');
        if(connectId) {
            getConnections().then(response => {
                connectionsSetInit(connectId);
            })
        }
    }

    const connectionsSetInit = (connectId = null) => {

        for(let id in storeData.connections) {
            storeData.connections[id]['is_active'] = false;
        }

        if(connectId) {
            storeData.connections[connectId]['is_active'] = true;
            storeData.connect_id = connectId;
            storeData.connect = storeData.connections[connectId];
            setActiveConnect(connectId, storeData.connect)
        }
    }

    const setActive =  (list, key = null, field = null) => {}

    return {
        ...useHttp(),
        query, freeSqlMake,setActiveConnect, setActive, reloadPage,
        notify: notifyMessage, notifyMessage, responseMessage, saveResponseHandle,
        getUsers, createUser, userPrivilegesChange,
        getTables, dropTable, copyTable, createTable, renameTable, getTableFields, dropField, addField,
        getDatabases, createDatabase, deleteDatabase, copyDatabase,
        getConnections, createConnect, updateConnect, deleteConnect, checkConnect,
    }
}