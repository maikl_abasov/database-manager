import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import { createPinia } from 'pinia'
const pinia = createPinia()

import '@/assets/plugins/global/plugins.bundle.css';
import '@/assets/css/style.bundle.css';
import '@/assets/style.css';

import Notifications from '@kyvg/vue3-notification'
import SmallTable from "./components/db-manager/SmallTable.vue";
import Preloader from './components/common/Preloader.vue'
import ItemToggle from './components/common/ItemToggle.vue'
import FormEditor from './components/common/FormEditor.vue'
import DragItem from './components/common/DragItem.vue'
import DragCard from './components/common/DragCard.vue'

const app = createApp(App)
app.use(pinia)
app.use(router)
app.use(Notifications)
app.component('preloader'  , Preloader);
app.component('item-toggle', ItemToggle);
app.component('small-table', SmallTable);
app.component('form-editor', FormEditor);
app.component('drag-item', DragItem);
app.component('drag-card', DragCard);
app.mount('#app')
