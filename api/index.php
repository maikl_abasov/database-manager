<?php

set_time_limit(0);

session_start();

define('ROOT_DIR', __DIR__);

require_once ROOT_DIR . '/vendor/autoload.php';

require_once ROOT_DIR . '/src/bootstrap.php';

use Dzion\Api\Core\App;
use Dzion\Api\App\SessionConfig;

try {

    SessionConfig::setConnectId();

    $response = (new App($_SERVER['PATH_INFO']))->handle();

} catch (Exception $err) {

    $response = exceptionHandle($err, 'index-exception.txt');

}

getResponse($response);







