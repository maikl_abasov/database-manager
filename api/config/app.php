<?php

use Dzion\Api\Core\Container\Container;
use Dzion\Api\Core\Container\ContainerInterface;
use Dzion\Api\App\SessionConfig;
use Dzion\Api\App\Connection;
use Dzion\Api\Core\DB;

$container = Container::getInstance();

$container->set('db_config', function(ContainerInterface $container) {
    $connectId = SessionConfig::getConnectId();
    return (new Connection())->getConnect($connectId);
});

$container->set('db', function(ContainerInterface $container) {
    $config = $container->get('db_config');
    return new Dzion\Api\Core\Database($config);
});
