<?php

use Dzion\Api\Core\Container\Container;

function app(string $key, mixed $params = null, bool $static = true) {
    return (Container::getInstance())->get($key, $params, $static);
}

function lg($data, $stopped = true) {
    $out = print_r($data, true);
    echo "<pre>{$out}</pre>";
    if($stopped) die();
}

function getResponse($data) {
    die(json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
}

function getServerHeader($headerName, $fn = null) {
    $data = null;
    $HEADERS = (array)getallheaders();
    if(!empty($HEADERS[$headerName])) $data = $HEADERS[$headerName];
    if($fn) return $fn($data);
    return $data;
}

function setCurrentConnectId() {
    // '35ff81045aa37e317d513883be326fc4' mysql;
    // '1af2fac71e1b2c5245dc9e69dfb09c24' pgsql
    $newConnectId = getServerHeader(HEADER_CONF_NAME);
    $_SESSION[SESSION_CONF_NAME] = $newConnectId;
}

function getCurrentConnectId() {
    return (!empty($_SESSION[SESSION_CONF_NAME])) ? $_SESSION[SESSION_CONF_NAME] : '';
}

//////////////////////
///
///

function commandDataParser($data) {
    $result = [];
    foreach ($data as $key => $value) {
        $item = explode(" ", $value);
        $list = [];
        foreach ($item as $i => $param) {
            $param = trim($param);
            if($param) $list[] = $param;
        }
        $result[$key] = $list;
    }
    return $result;
}

function makeCommand($COMMAND, $sshConnect) {
    $stream = ssh2_exec($sshConnect->connect, $COMMAND);
    stream_set_blocking($stream, true);
    $content = stream_get_contents($stream);
    $content = trim($content);
    $stderr_stream   = ssh2_fetch_stream($stream, SSH2_STREAM_STDERR);
    $content_error   = stream_get_contents($stderr_stream);
    $result = explode("\n", $content);
    return [
        'result' => $result,
        'error'  => $content_error,
    ];
}

function sshConnect($HOST, $USER, $PASSWORD, $PORT) {

    $error = null;
    $message = null;

    try {

        $connection = ssh2_connect($HOST, $PORT);
        $auth = ssh2_auth_password($connection, $USER, $PASSWORD);
        $sftp = ssh2_sftp($connection);

    } catch (Exception $err) {
        $message = $err->getMessage();
        $error = true;
    }

    $connect = new stdClass();
    $connect->connect = $connection;
    $connect->sftp = $sftp;
    $connect->auth = $auth;
    $connect->error = $auth;
    $connect->errorMessage = $message;

    return $connect;
}

//////////////////////////
///
///
///

function getInputData() {
    $rawData = file_get_contents("php://input");
    $result =  (array)json_decode($rawData);
    if(empty($result)) return [];
    return $result;
}

function getError($data) {
    getResponse($data, 'error');
}

function getDebug($data) {
    getResponse($data, 'debug');
}


function routerInit($fName = 'PATH_INFO') {
    $server = $_SERVER;
    $routes = array();
    if(!empty($server[$fName])) {
        $pathInfo = $server[$fName];
        $pathInfo = trim($pathInfo, '/');
        $routes   = explode('/', $pathInfo);
    }

    return $routes;
}

function saveConfig($newData, $oldData, $fileName = 'config/conf.php', $confDir = 'config') {

    $newStringData = renderArrayToString($newData);
    $oldStringData = renderArrayToString($oldData);

    // сохраняем старый конфиг
    $oldFileName = $confDir . '/old_config/conf_' . date("Y_m_d___H_i_s") . '.txt';

    if(empty($oldData))  return false;

    file_put_contents($oldFileName, $oldStringData);

    if(empty($newData))  return false;

    // перезаписываем новый конфиг
    file_put_contents($fileName, $newStringData);

    return true;
}

function renderArrayToString($arrData) {
    $ch = 0;
    $stringData = "";
    foreach ($arrData as $key => $value) {
        ($ch) ? $sep = ',' :  $sep = '';
        $stringData .= "{$sep}'{$key}' => '{$value}' \n";
        $ch++;
    }
    $stringData = "<?php \n return array(\n {$stringData}); \n";
    return $stringData;
}

function s2($data) {

    $fileName = "my_file.php";
    $stringData = serialize($data);
    file_put_contents($fileName, $stringData);
    $stringData = file_get_contents($fileName);
    $configData = unserialize($stringData);
    return $configData;
}


function textAreaContentRender($text) {
    $result = explode("\n", $text);
    return $result;
}


function lgTrace() {

    $debugTrace = debug_backtrace();
    $args = func_get_args();

    $get = false;
    $output = $traceStr = '';

    $style = 'margin:10px; padding:10px; border:3px red solid;';

    foreach ($args as $key => $value) {
        $itemArr = array();
        $itemStr = '';
        is_array($value) ? $itemArr = $value : $itemStr = $value;
        if ($itemStr == 'get') $get = true;
        $line = print_r($value, true);
        $output .= '<div style="' . $style . '" ><pre>' . $line . '</pre></div>';
    }

    foreach ($debugTrace as $key => $value) {
        $itemArr = array();
        $itemStr = '';
        is_array($value) ? $itemArr = $value : $itemStr = $value;
        if ($itemStr == 'get') $get = true;
        $line = print_r($value, true);
        $output .= '<div style="' . $style . '" ><pre>' . $line . '</pre></div>';
    }


    if ($get)  return $output;
    print $output;
    die ;
}

function memory_usage() {
    $mem_usage = memory_get_usage(true);
    if ($mem_usage < 1024) $mem_usage .= ' b';
    elseif ($mem_usage < 1048576) $mem_usage = round($mem_usage/1024,2) . ' kb';
    else $mem_usage = round($mem_usage/1048576,2) . ' mb';
    return $mem_usage;
}

function custom_fatal_error_hanler() {

    $error = error_get_last();

    if (is_array($error) &&  in_array($error['type'], [E_ERROR, E_PARSE, E_CORE_ERROR, E_COMPILE_ERROR])) {
        while (ob_get_level()) {
            ob_end_clean();
        }

        $errorTypes = array (
            E_ERROR              => 'Фатальная ошибка',
            E_WARNING            => 'Предупреждение',
            E_PARSE              => 'Ошибка разбора исходного кода',
            E_NOTICE             => 'Уведомление',
            E_CORE_ERROR         => 'Ошибка ядра',
            E_CORE_WARNING       => 'Предупреждение ядра',
            E_COMPILE_ERROR      => 'Ошибка на этапе компиляции',
            E_COMPILE_WARNING    => 'Предупреждение на этапе компиляции',
            E_USER_ERROR         => 'Пользовательская ошибка',
            E_USER_WARNING       => 'Пользовательское предупреждение',
            E_USER_NOTICE        => 'Пользовательское уведомление',
            E_STRICT             => 'Уведомление времени выполнения',
            E_RECOVERABLE_ERROR  => 'Отлавливаемая фатальная ошибка'
        );

        $errNo      = $error['type'];
        $errMessage = $error['message'];
        $errFile    = $error['file'];
        $errLine    = $error['line'];
        $errConst   = $errorTypes[$errNo];
        $requestUri = $_SERVER['REQUEST_URI'];

        $today = date('Y.m.d__H:i:s');
        $errorLog  = "Date : {$today} \n";
        $errorLog .= "Type : PHP ERROR \n";
        $errorLog .= "RequestUri : {$requestUri} \n";
        $errorLog .= "Title: Обработчик ошибок (allErrorHanler) \n";
        $errorLog .= "Place: line={$errLine}; file={$errFile}\n";
        $errorLog .= "ErrNo: {$errNo} \n";
        $errorLog .= "ErrConst: {$errConst} \n";
        $errorLog .= "Message :  {$errMessage}\n";
        $errorLog .= "\n";

        logFileSave($errorLog);
    }
}

function error_exception_handler($exception) {
    echo "Исключение: <br>";
    echo "Message: " , $exception->getMessage(), "<br>";
    echo "Line: " , $exception->getLIne(), "<br>";
    echo "File: " , $exception->getFile(), "<br>";
}

function logFileSave($logData, $logFileName = 'fatal-error.txt') {
    $logFilePath = LOGS_DIR . '/' . $logFileName;
    $data = $logData;
    if(file_exists($logFilePath)) {
        $current = file_get_contents($logFilePath);
        $data = $logData ."\n". $current;
    }
    return file_put_contents($logFilePath, $data,  LOCK_EX);
}


//--- Функция обработки ошибок
function customErrorWarningHandler($errNo, $errMessage, $errFile, $errLine) {

    // может потребоваться экранирование $errstr:
    $errMessage = htmlspecialchars($errMessage);
    $requestUri = $_SERVER['REQUEST_URI'];

    $today = date('Y.m.d__H:i:s');
    $errorLog  = "Date : {$today} \n";
    $errorLog .= "Type : PHP ERROR \n";
    $errorLog .= "RequestUri : {$requestUri} \n";
    $errorLog .= "Title: Обработчик ошибок (customErrorWarningHandler) \n";
    $errorLog .= "Place: line={$errLine}; file={$errFile}\n";
    $errorLog .= "ErrNo: {$errNo} \n";
    $errorLog .= "Message :  {$errMessage}\n";
    $errorLog .= "\n";

    logFileSave($errorLog);

}

function exceptionHandle($err, $fileName = 'index-exception.txt') {

    $error = [
        'message' => $err->getMessage(),
        'file'  => $err->getFile(),
        'line'  => $err->getLine(),
        'code'  => $err->getCode(),
        'trace' => $err->getTrace(),
        'time'  => date("Y-m-d H:i:s"),
    ];

    $errorData = "//------------------------ \n";
    foreach ($error as $key => $value) {
        if($key == 'trace') continue;
        $errorData .= "$key => $value \n";
    }
    $errorData .= "//------------------------ \n";

    logFileSave($errorData, $fileName);

    return ['error' => $error];
}

function curlJsonPost($url, $postData, $connectId = '') {

    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-CONNECT-ID:' . $connectId ));
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
    // curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postData, JSON_UNESCAPED_UNICODE));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_HEADER, false);
    $response = curl_exec($ch);
    $info = curl_getinfo($ch);

    if(curl_errno($ch)) {
        $error = ['method' => 'GET', 'url' => $url, 'error' => $info,];
        lg($error);
    }
    curl_close($ch);

    // $response = json_encode($response, JSON_UNESCAPED_UNICODE);
    return ['result' => $response, 'info' => $info];

}

function curlGet($url, $connectId = '') {
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-CONNECT-ID:' . $connectId ));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_HEADER, false);
    $response = curl_exec($ch);
    $info = curl_getinfo($ch);

    if(curl_errno($ch)) {
        $error = ['method' => 'GET', 'url' => $url, 'error' => $info,];
        lg($error);
    }
    curl_close($ch);

    return ['result' => $response, 'info' => $info];
}

function curlPost($url, $postData){
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postData, '', '&'));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_HEADER, false);
    $response = curl_exec($ch);
    curl_close($ch);
    return $response;
}