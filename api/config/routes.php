<?php

// (http://host.ru) (db-manager/api/index.php) PATH_INFO=(/db-manager/databases/list)
// http://host.ru/db-manager/api/index.php/db-manager/databases/list

use Dzion\Api\Core\Router;
use Dzion\Api\App\DatabaseManager\DbServiceFactory;
use Dzion\Api\App\Connection;
use Dzion\Api\Migrate\FileImporter;
use Dzion\Api\Migrate\FileExporter;
use Dzion\Api\Core\DB;
use Dzion\Api\Core\Database;

Router::route('/connection/list', function() {
    $response = (new Connection())->getConnections();
    return $response;
});

Router::route('/connection/create', function() {
    $response = (new Connection())->createConnect();
    return $response;
});

Router::route('/connection/delete/(\w+)', function($key) {
    $response = (new Connection())->deleteConnect($key);
    return $response;
});

Router::route('/connection/update/(\w+)', function($key) {
    $response = (new Connection())->updateConnect($key);
    return $response;
});

Router::route('/connection/check-connect/{connect_id}', function($connectId) {

    $config = (new Connection)->getConnect($connectId);

    $response['connect-status'] = false;
    $response['connect-message'] = 'Не удалось подключиться';

    try {
        $pdo = new DB($config);
        if(!empty($pdo)) {
            $response['connect-status'] = true;
            $response['connect-message'] = 'Успешное подключение';
            $response['connect-pdo'] = $pdo;
        }
    } catch (Exception $err) {
        $response['connect-error'] = $err->getMessage();
    }

    return $response;
});

Router::route('/db-manager/databases/list', function() {
    $db = app('db');
    $databaseService = (new DbServiceFactory($db))->getService();
    return $databaseService->getDatabaseList();
});

Router::route('/db-manager/tables/list/(\w+)', function($dbname) {
    $db = app('db');
    $db->setDbName($dbname, true);
    $databaseService = (new DbServiceFactory($db))->getService();
    return $databaseService->getTables();
});

Router::route('/db-manager/get/{action}', function($action) {

    $params = $_GET;
    $db = app('db');
    $databaseService = (new DbServiceFactory($db, $params))->getService();

    $response = [];
    switch ($action) {
        case 'get_table_fields' :
            $db->setDbName($params['db'], true);
            $databaseService->setDatabase($db);
            $response = $databaseService->getFields($params['name']);
            break;

        default :
            $sqlCommands = require_once CONFIG_DIR . '/sql_commands.php';
            if(!empty($sqlCommands[$action])) {
                $command = $sqlCommands[$action];
                $response = $databaseService->makeSqlCommand($command, $action);
            }
            break;
    }

    return $response;
});


Router::route('/db-manager/post/{action}', function($action) {

    $params = $_POST;
    $db = app('db');
    $databaseService = (new DbServiceFactory($db, $params))->getService();

    $response = [];
    switch ($action) {
        default :
            $sqlCommands = require_once CONFIG_DIR . '/sql_commands.php';
            if(!empty($sqlCommands[$action])) {
                $command = $sqlCommands[$action];
                $response = $databaseService->makeSqlCommand($command, $action);
            }
            break;
    }

    return $response;
});

//Router::route('/db-manager/post/{action}', function($action) use($SQL_COMMANDS, $config) {
//    $param = $_POST;
//    $manager = new DbManager($SQL_COMMANDS, $config);
//    $response = $manager->make($action, $param);
//    return $response;
//});

Router::route('/db-manager/free-sql-make', function() {
    $sql  = $_POST['sql'];
    $type = $_POST['type'];
    $sqlCommands = require_once CONFIG_DIR . '/sql_commands.php';
    $config = app('db_config');
    $manager = new DbManager($sqlCommands, $config);
    $response = $manager->freeSqlmake($sql, $type);
    return $response;
});

Router::route('/db-migrate/get-migrate-files', function() {

    $config = app('db_config');
    $db = new Database($config);
    $migrate = new FileImporter($db, MIGRATIONS_DIR);
    $files = $migrate->scanDir(MIGRATION_FILES . '/*', true);
    return $files;
});

Router::route('/db-migrate/get-migrate-item', function() {

    //$sqlCommands = require_once CONFIG_DIR . '/sql_commands.php';
    $migrateFile = $_GET['migrate_file'];
    $config = app('db_config');
    $db = new Database($config);
    $migrate = new FileImporter($db, MIGRATIONS_DIR);
    $migratePath = MIGRATION_FILES . '/' . $migrateFile;
    $tables = $migrate->scanDir($migratePath . '/tables/*', true);
    $data = $migrate->scanDir($migratePath . '/data/*', true);

    return [
       'tables' => $tables,
       'data' => $data,
    ];
});

Router::route('/db-migrate/export', function() {
    $config = app('db_config');
    $db = new Database($config);
    $fileSaver = new \Dzion\Api\Services\FileSaveService();
    $export = new FileExporter($db, MIGRATIONS_DIR, $fileSaver);
    $response = $export->run();
    return $response;
});

Router::route('/db-migrate/import', function()  {
    $migrateFile = $_GET['migrate_file'];
    $config = app('db_config');
    $db = new Database($config);
    $import = new FileImporter($db, MIGRATIONS_DIR);
    $response = $import->run($migrateFile);
    return $response;
});


//Router::addRoute('get-files', function()  {
//    $response = (new SSH2_CLASS())->scandir();
//    return $response;
//});
//
//Router::addRoute('search-files', function()  {
//    $response = (new SSH2_CLASS())->searchFiles();
//    return $response;
//});
//
//Router::addRoute('make-command', function() {
//    $response = (new SSH2_CLASS())->makeCommand();
//    return $response;
//});
//
//Router::addRoute('get-connect-list', [Connection::class, 'getConnectList']);
//Router::addRoute('create-connect', [Connection::class, 'createConnect']);
//Router::addRoute('update-connect', [Connection::class, 'updateConnect']);
//Router::addRoute('check-connect', [Connection::class, 'checkConnect']);
//Router::addRoute('delete-connect', [Connection::class, 'deleteConnect']);
