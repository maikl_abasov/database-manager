<?php

//SHOW DATABASES; - список баз данных
//SHOW TABLES [FROM db_name]; -  список таблиц в базе
//SHOW COLUMNS FROM таблица [FROM db_name]; - список столбцов в таблице
//SHOW CREATE TABLE table_name; - показать структуру таблицы в формате "CREATE TABLE"
//SHOW INDEX FROM tbl_name; - список индексов
//SHOW GRANTS FOR user [FROM db_name]; - привилегии для пользователя.
//
//
//SHOW VARIABLES; - значения системных переменных
//SHOW [FULL] PROCESSLIST; - статистика по mysqld процессам
//SHOW STATUS; - общая статистика
//SHOW TABLE STATUS [FROM db_name]; - статистика по всем таблицам в базе

return  [

    // ---- DATABASE ---
    // -- Получаем все базы на сервере
    'databases' => [
        'pgsql' => 'SELECT * FROM pg_database',
        'mysql' => 'SHOW DATABASES',
        'param' => [],
        'type'  => 'query',
    ],

    'create_database' => [
        'pgsql' => 'CREATE DATABASE dbname',
        'mysql' => 'CREATE DATABASE dbname',
        'param' => ['dbname'],
        'type'  => 'exec',
    ],

    'delete_database' => [
        'pgsql' => 'DROP DATABASE dbname',
        'mysql' => 'DROP DATABASE dbname',
        'param' => ['dbname'],
        'type'  => 'exec',
    ],

    'copy_database' => [
        'pgsql' => 'CREATE DATABASE new_name WITH TEMPLATE dbname',
        'mysql' => '',
        'param' => ['dbname', 'new_name'],
        'type'  => 'exec',
    ],

    // --- USERS ---
    // получаем всех пользователей
    'users' => [
        'pgsql' => 'SELECT * FROM pg_user',
        'mysql' => 'SELECT * FROM mysql.user',
        'param' => [],
        'type'  => 'query',
    ],

    'create_user' => [
        'mysql' => "CREATE USER 'name'@'%' IDENTIFIED BY 'password'",
        'pgsql' => "CREATE USER name WITH PASSWORD 'password'",
        'param' => ['name', 'password'],
        'type'  => 'exec',
    ],

    'set_user_privileges' => [
        'mysql' => "",
        'pgsql' => "GRANT ALL PRIVILEGES ON DATABASE dbname TO user",
        'param' => ['dbname', 'user'],
        'type'  => 'exec',
    ],

    'delete_user_privileges' => [
        'mysql' => "",
        'pgsql' => "REVOKE ALL PRIVILEGES ON DATABASE dbname FROM user",
        'param' => ['dbname', 'user'],
        'type'  => 'exec',
    ],

    // --- TABLES ---
    'tables' => [
        'pgsql' => "SELECT * FROM information_schema.tables WHERE table_schema = 'public' AND table_catalog = 'dbname' ",
        'mysql' => "SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbname' AND TABLE_TYPE = 'BASE TABLE'",
        'param' => ['dbname'],
        'type'  => 'query',
    ],

    'create_table' => [
        'mysql' => 'CREATE TABLE name (id int PRIMARY KEY AUTO_INCREMENT)',
        'pgsql' => 'CREATE TABLE name (id SERIAL PRIMARY KEY)',
        'param' => ['name', 'id'],
        'type'  => 'exec',
    ],

    'drop_table' => [
        'mysql' => 'DROP TABLE name',
        'pgsql' => 'DROP TABLE name',
        'param' => ['name'],
        'type'  => 'exec',
    ],

    'copy_table' => [
        'mysql' => 'CREATE TABLE new_name SELECT * FROM old_name',
        'pgsql' => 'CREATE TABLE new_name AS (SELECT * FROM old_name)',
        'param' => ['old_name', 'new_name'],
        'type'  => 'exec',
    ],

    'rename_table' => [
        'mysql' => 'RENAME TABLE old_name TO new_name',
        'pgsql' => 'ALTER TABLE old_name RENAME TO new_name',
        'param' => ['old_name', 'new_name'],
        'type'  => 'exec',
    ],

    'get_table_fields' => [
        'mysql' => "SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE `TABLE_NAME` = 'name' AND `TABLE_SCHEMA` = 'db'",
        'pgsql' => "SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'name'",
        'param' => ['name', 'db'],
        'type'  => 'query',
    ],

    'rename_column' => [
        'mysql' => 'ALTER TABLE table RENAME COLUMN old_name TO new_name',
        'pgsql' => '',
        'param' => ['table', 'old_name', 'new_name'],
        'type'  => 'exec',
    ],

    'add_field' => [
        'mysql' => 'ALTER TABLE table ADD column_name data_type size DEFAULT NULL',
        'pgsql' => "ALTER TABLE table ADD COLUMN  column_name data_type size DEFAULT NULL",
        'param' => ['table', 'column_name', 'data_type', 'size'],
        'type'  => 'exec',
    ],

    'drop_field' => [
        'mysql' => 'ALTER TABLE table_name DROP COLUMN field_name',
        'pgsql' => 'ALTER TABLE table_name DROP COLUMN field_name',
        'param' => ['table_name', 'field_name'],
        'type'  => 'exec',
    ],

    'update_field_type' => [
        'mysql' => 'ALTER TABLE table MODIFY COLUMN name type',
        'pgsql' => '',
        'param' => ['table', 'name', 'type'],
        'type'  => 'exec',
    ],

];
