<?php

namespace Dzion\Api\Migrate\tests;

class FileImporterTest extends DbMigrateBase
{

    public function __construct(array $config, $migrationsFiles)
    {
        parent::__construct($config, $migrationsFiles);
    }

    public function run($migrateFile)
    {
        $this->migrateFileDir = $this->rootMigrationFiles . '/' . $migrateFile; // устанавливаем директорию которую будем читать
        $tableFiles = $this->scanDir($this->migrateFileDir . '/tables/*'); // получаем файлы таблиц
        $dataFiles = $this->scanDir($this->migrateFileDir . '/data/*'); // получаем файлы таблиц

        $startDate = date('H:i:s');

        $tables = $this->importTables($tableFiles);

        $response = $this->importTablesData($dataFiles, $tables);

        $endDate = date('H:i:s');
        $time = ['start' => $startDate, 'end' => $endDate];

        return [
            'time' => $time,
            'tables' => $tables,
            'data' => $response,
        ];
    }

    public function createTable(string $tableName, array $fields)
    {
        $status = false;
        $addFields = $insertFields = [];
        $wrap = ($this->driver == 'pgsql') ? '"' : '`';

        foreach ($fields as $key => $item) {

            $name = $item['column_name'];
            $type = $item['data_type'];
            $insertFields[] = $name;

            $primaryValue = $this->setPrimaryKey($item);
            if (!empty($primaryValue)) {
                $addFields[] = $primaryValue['sql'];
                continue;
            }

            $type = $this->setType($type);
            $fieldSql = $wrap . $name . $wrap . ' ' . $type . ' DEFAULT NULL';
            $addFields[] = $fieldSql;

        }

        $fieldsLine = implode(", \n", $addFields);
        $createTableQuery = "DROP TABLE IF EXISTS {$tableName};
                             CREATE TABLE {$tableName} ({$fieldsLine});";
        $this->exec($createTableQuery);

        return [
            'status' => $status,
            'insert_fields' => $insertFields
        ];
    }

    public function importTables(array $tableFiles)
    {
        $tables = [];
        foreach ($tableFiles as $tableFile) {
            $fields = (array)json_decode(file_get_contents($tableFile), true);
            $tableName = $this->getTableName($tableFile);
            $tables[$tableName] = $this->createTable($tableName, $fields);
            $tables[$tableName]['fields'] = $fields;
        }
        return $tables;
    }

    public function importTablesData(array $dataFiles, array $tables)
    {

        $response = [];
        foreach ($dataFiles as $tableFile) {

            $tableName = $this->getTableName($tableFile);
            $insertFields = $tables[$tableName]['insert_fields'];

            //print "$tableName <br>";
            //if ($tableName != 'avc_additional_properties') continue;

            $count = 0;
            $error = [];

            $limit = $this->importLimit;
            $delimiter = $this->delimiter;

            // $s1 = date('H:i:s');

            $data = file($tableFile);
            // $count = count($data);

            if (count($data) > 10000) {
                $data = array_chunk($data, 700);
            }

            foreach ($data as $list) {

                $chunkData = [];
                $row = 0;
                foreach ($list as $value) {
                    $row++;
                    $chunkData[] = explode($delimiter, $value);
                }

                $save = $this->importData($tableName, $chunkData, $insertFields);
                if (!empty($save['error'])) {
                    $error[] = $save['error'];
                } else  {
                    $count = $count + $row;
                }
            }

//            lg([
//                $s1,
//                date('H:i:s'),
//                $count,
//                $data[1],
//            ]);

            $response[$tableName] = [
                'count' => $count,
                'error' => $error,
            ];
        }

        return $response;
    }

    protected function importData($tableName, array $data, array $fields)
    {

        $wrap = ($this->driver == 'pgsql') ? '"' : '`';
        foreach ($fields as $key => $name) {
            $fields[$key] = $wrap . $name . $wrap;
        }

        $valuesData = $bindsData = [];
        foreach ($data as $key => $item) {
            $valueItem = $bindItem = [];
            foreach ($item as $i => $value) {
                $name = ':' . $key . '_' . $i;
                $valueItem[$name] = $name;
                $bindItem[$name] = $value;
            }
            $valuesData[] = '(' . implode(',', $valueItem) . ')';
            $bindsData[] = $bindItem;
        }

        $fieldsSql = implode(',', $fields);
        $valuesSql = implode(',', $valuesData);
        $query = 'INSERT INTO ' . $tableName . '(' . $fieldsSql . ') VALUES' . $valuesSql;

        $message = '';
        $stmt = $this->pdo->prepare($query);
        try {
            $this->pdo->beginTransaction();
            foreach ($bindsData as $item) {
                foreach ($item as $name => $value) {
                    if (!$value) $value = NULL;
                    $stmt->bindValue($name, $value);
                }
            }

            $stmt->execute();
            $this->pdo->commit();
        } catch (Exception $e) {
            $this->pdo->rollback();
            $message = $e->getMessage();
        }

        return [
            'error' => $message,
        ];
    }

    protected function getTableName($tableFile)
    {
        $fileInfo = new SplFileInfo($tableFile);
        $tableName = explode('.', $fileInfo->getFilename())[0];
        return $tableName;
    }

    protected function setType(string $type): string
    {
        $resultType = "VARCHAR(250)";
        $driver = $this->driver;
        if ($driver == 'mysql') $resultType = $this->resolveType($type);
        else $resultType = $this->resolveType($type);
        if ($resultType == 'varchar' || $resultType == 'VARCHAR')
            $resultType = $resultType . '(250)';
        return $resultType;
    }

    protected function setPrimaryKey(array $field): array|bool
    {
        $sql = $name = false;
        if ((!empty($field['extra'])) && ($field['extra'] == 'auto_increment')) {
            $name = $field['column_name'];
        } elseif (!empty($field['column_key'] && $field['column_key'] == 'PRI')) {
            $name = $field['column_name'];
        } else {
            if (!empty($field['column_default'])) {
                $pos = strpos($field['column_default'], 'nextval');
                if ($pos !== false) {
                    $name = $field['column_name'];
                }
            }
        }

        if (!$name) return $sql;

        if ($this->driver == 'mysql') $sql = "{$name} int PRIMARY KEY AUTO_INCREMENT";
        else $sql = "{$name} SERIAL PRIMARY KEY";

        return [
            'name' => $name,
            'sql' => $sql
        ];
    }

    protected function resolveType($type)
    {

        $types = [
            'integer' => 'int',
            'character varying' => 'varchar',
            'timestamp without time zone' => 'varchar',
            'smallint' => 'int',
            'tinytext' => 'text',
            'tinyint' => 'int',
            'char' => 'varchar',
            'bigint' => 'bigint',
            'longtext' => 'text',
            'enum' => 'varchar',
            'double' => 'varchar',
            'datetime' => 'varchar',
            'timestamp' => 'varchar',
            'TIMESTAMP' => 'varchar',
        ];

        return (isset($types[$type])) ? $types[$type] : $type;
    }

}
