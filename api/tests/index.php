<?php

set_time_limit(0);

session_start();

define('ROOT_DIR', dirname(__DIR__));

require_once ROOT_DIR . '/vendor/autoload.php';

require_once ROOT_DIR . '/src/bootstrap.php';

$urlList = explode('/', trim($_SERVER['REQUEST_URI'], '/'));
array_pop($urlList);
$apiUrl = implode('/', $urlList);
$apiUrl = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'] . '/' . $apiUrl;
define('API_URL', $apiUrl . '/index.php');

function show($info, $response = [])
{
    $info = print_r($info, true);
    $data = (!empty($response)) ? "<pre>" . print_r($response, true) . "</pre>" : '';
    echo "<div style='border-bottom: 2px green dotted'>
             <pre>{$info}</pre>
              {$data}
          </div> <br>";
}

function decode($data)
{
    return (array)json_decode($data);
}

// http://my-web-service/db-manager/api/tests/

// $CONNECT_ID = '69ad3ac54ae617823d196b7bef55ca0d'; // mysql
// $CONNECT_ID = 'db76462e15ae9f1940f9720b7de6152d'; // mysql base_mysql
$CONNECT_ID = '29fa17c951f15caf13cbea218a8b2d58';    // postgres test_pg1
// $CONNECT_ID = '96de0463cdb125271d7a33f44318fe49';    // postgres d44


/////////////////////////////////////
///
///  START TESTS
///

$config = [
    "host" => "127.0.0.1",
    "dbname" => "test_pg1",
    "user" => "postgres",
    "password" => "34",
    "driver" => "pgsql",
    "port" => "5432",
    "is_active" => "true"
];

use Dzion\Api\Migrate\FileImporter;
use Dzion\Api\Core\Database;
use Dzion\Api\Services\MemoryUsageInfo;

class MigrateImporter extends FileImporter
{

    public function dataFilesImport(array $dataFiles, array $tables): array
    {
        $response = [];
        foreach ($dataFiles as $tableFile) {
            $tableName = $this->getTableName($tableFile);
            $fields = $tables[$tableName]['insert_fields'];
            $iterator = $this->readCsv($tableFile);
            foreach ($iterator as $data) {
                $response[] = $this->insertData($tableName, $data, $fields);
            }
        }
        return $response;
    }

    protected function readCsv(string $tableFile)
    {
        $row = 1;
        $handle = fopen($tableFile, "r");
        if ($handle !== FALSE) {
            $data = [];
            while (($item = fgetcsv($handle, 0, ";")) !== FALSE) {
                $row++;
                $data[] = $item;

                if ($row > 5) {
                    $row = 1;
                    $response = $data;
                    $data = [];
                    yield $response;
                }
            }

            if (!empty($data)) {
                yield $data;
            }
        }
        fclose($handle);
    }


    public function export_csv($tableName, $tableFile, $fields) {
        $row = 1;
        $data = [];
        $fullData = explode("\n", file_get_contents($tableFile));
        foreach ($fullData as $line) {
            $row++;
            $data[] = explode(";", $line);
            if($row > 10) {
                $this->insertData($tableName, $data, $fields);
                $row = 1;
                $data = [];
            }
        }
    }

}

$dataPath = MIGRATIONS_DIR . '/files/127.0.0.1__base_mysql/data';
$tablesPath = MIGRATIONS_DIR . '/files/127.0.0.1__base_mysql/tables';

$tableFiles = [
    //$tablesPath . '/cbn_avito_data.json',
    //$tablesPath . '/autobasebuy_1.json',
    $tablesPath . '/avc_additional_properties.json',
    //$tablesPath . '/cbn_connects.json',
    //$tablesPath . '/pay_session.json',
];

$dataFiles = [
    //$dataPath . '/cbn_avito_data.json',
    //$dataPath . '/autobasebuy_1.json',
    $dataPath . '/avc_additional_properties.json',
    //$dataPath . '/cbn_connects.json',
    //$dataPath . '/pay_session.csv',
];

$db = new Database($config);

//$import = new MigrateImporter($db, MIGRATIONS_DIR);
//$response = $import->dataFilesRender($dataFiles);

//$memory = new MemoryUsageInfo(true);
//$memory->setStart('Старт');
//
////$import = new FileImporter($db, MIGRATIONS_DIR);
////$memory->setStart('Загружаем таблицы');
////$tables = $import->importTables($tableFiles);
////$memory->setStart('Загружаем данные');
////$data = $import->importTablesData($dataFiles, $tables);
//
//$import = new MigrateImporter($db, MIGRATIONS_DIR);
//$memory->setStart('Загружаем таблицы');
//$tables = $import->importTables($tableFiles);
//
//$memory->setStart('Загружаем данные');
//$data = $import->dataFilesImport($dataFiles, $tables);
//
//$memory->setEnd("Финиш");
//$info = $memory->printMemoryUsageInfo();
//
//lg([
//    'info' => $info,
//    'data' =>$data
//]);

//$import = new MigrateImporter($db, MIGRATIONS_DIR);
//$tables = $import->importTables($tableFiles);
//$data = $import->importTablesData($dataFiles, $tables);
//
//lg($data);

//// $migrateFile = '127.0.0.1__base_mysql';
//
//$filePath = MIGRATIONS_DIR . '/files/127.0.0.1__base_mysql/data/cbn_avito_data.json';
//
//$row = 1;
//if (($handle = fopen($filePath, "r")) !== FALSE) {
//    $data = [];
//    while (($item = fgetcsv($handle, 0, ";")) !== FALSE) {
//        $row++;
//        $data[] = $item;
//
//        if($row > 5) {
//           lg($data);
//        }
//    }
//    fclose($handle);
//}
//
//
//lg($import);

// ---- создание базы из файлов
// $url = '/db-migrate/import?migrate_file=127.0.0.1__d44'; // pgsql
// $url = '/db-migrate/import?migrate_file=127.0.0.1__instant_cms'; // mysql
$url = '/db-migrate/import?migrate_file=127.0.0.1__base_mysql'; // mysql
$data = curlGet(API_URL . $url, $CONNECT_ID);

lg($data['result']);

// ---- выгрузка базы в файлы
$url = '/db-migrate/export';
$data = curlGet(API_URL . $url, $CONNECT_ID);

lg($data['result']);


///---------------------------------/
$url = '/connection/list';
$data = curlGet(API_URL . $url);
if (!empty($data['result'])) {
    $info = $data['info'];
    $result = $data['result'];
    show(['url' => $url, 'info' => 'ok']);
} else {
    show(['url' => $url], $data);
    die('ERROR');
}

///---------------------------------
$post = [
    "host" => "127.0.0.1",
    "dbname" => "test_db_name_1",
    "user" => "test_user_1",
    "password" => "1234_1",
    "driver" => "pgsql",
    "port" => "5432",
];

$url = '/connection/create';
$data = curlJsonPost(API_URL . $url, $post);
$result = decode($data['result']);

if (!empty($result['index'])) {
    $info = $data['info'];
    $list = (array)$result['list'];
    show(['url' => $url, 'info' => 'ok']);
} else {
    show(['url' => $url], $data);
    die('ERROR');
}

///---------------------------------
$update = [
    "host" => "127.0.0.1",
    "dbname" => "test_db_name_2",
    "user" => "test_user_2",
    "password" => "1234_2",
    "driver" => "pgsql",
    "port" => "5432",
];

$itemIndex = $result['index'];

$url = '/connection/update/' . $itemIndex;
$data = curlJsonPost(API_URL . $url, $update);
$result = decode($data['result']);

if (!empty((array)$result['list'])) {
    $info = $data['info'];
    $list = (array)$result['list'];
    show(['url' => $url, 'info' => 'ok']);
} else {
    show(['url' => $url], $data);
    die('ERROR');
}

///---------------------------------
$url = '/connection/delete/' . $itemIndex;
$data = curlGet(API_URL . $url);

if (!empty($data['result'])) {
    $info = $data['info'];
    $list = (array)$result['list'];
    show(['url' => $url, 'info' => 'ok']);
} else {
    show(['url' => $url], $data);
    die('ERROR');
}

///---------------------------------
$url = '/connection/check-connect/' . $CONNECT_ID;
$data = curlGet(API_URL . $url);
$result = decode($data['result']);

if (!empty($result['connect-status'])) {
    $info = $data['info'];
    $list = (array)$result['list'];
    show(['url' => $url, 'info' => 'ok']);
} else {
    show(['url' => $url], $data);
    die('ERROR');
}


///---------------------------------
$url = '/db-migrate/get-migrate-files';
$data = curlGet(API_URL . $url, $CONNECT_ID);
if (is_string($data['result'])) {
    if (str_contains($data['result'], 'Message')) {
        print $url;
        lg($data['result']);
    }
}

$files = decode($data['result']);

show(['url' => $url, 'info' => 'ok']);

///---------------------------------
$url = '/db-migrate/get-migrate-item?migrate_file=' . $files[0]->name;
$data = curlGet(API_URL . $url, $CONNECT_ID);

if (is_string($data['result'])) {
    if (str_contains($data['result'], 'Message')) {
        print $url;
        lg($data['result']);
    }
}

show(['url' => $url, 'info' => 'ok']);



