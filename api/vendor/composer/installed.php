<?php return array(
    'root' => array(
        'name' => 'dzion/api',
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'reference' => 'f740e1256cad8cdd6f661ffcdbd2e851d15000da',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        'dzion/api' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'reference' => 'f740e1256cad8cdd6f661ffcdbd2e851d15000da',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'josantonius/json' => array(
            'pretty_version' => 'v2.0.8',
            'version' => '2.0.8.0',
            'reference' => '617c10650ba03597b4294e1c4984f27df20958ef',
            'type' => 'library',
            'install_path' => __DIR__ . '/../josantonius/json',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
