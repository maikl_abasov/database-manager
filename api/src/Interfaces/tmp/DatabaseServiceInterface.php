<?php

namespace Dzion\Api\Interfaces;

interface DatabaseServiceInterface
{
    public function createDatabase(string $dbName);
}