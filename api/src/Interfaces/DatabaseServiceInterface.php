<?php

namespace Dzion\Api\Interfaces;

interface DatabaseServiceInterface
{
    public function makeSqlCommand(array $command, string $action) : array;
}