<?php

namespace Dzion\Api\Interfaces;

interface DBInterface
{
    public function connect(array $config = []) : void;
}