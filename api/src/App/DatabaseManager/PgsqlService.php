<?php

namespace Dzion\Api\App\DatabaseManager;

class PgsqlService extends DatabaseService
{

    // -- Получаем все базы на сервере
    public function getDatabaseList() : array
    {
        $sql =  "SELECT * FROM pg_database;";
        $list = $this->db->query($sql);
        return $this->dbListRender($list);
    }

    // -- Получить список таблиц
    public function getTables($scheme = 'public') : array
    {
        $sql = "SELECT * FROM information_schema.tables WHERE table_schema = '{$scheme}'";
        $list = $this->db->query($sql);
        return $this->tableListRender($list);
    }

    // -- Получить список полей таблицы
    public function getFields(string $table) : array {
        $sql = "SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '$table'";
        $list = $this->db->query($sql);
        return $this->fieldsRender($list);
    }

}