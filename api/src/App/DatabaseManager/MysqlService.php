<?php

namespace Dzion\Api\App\DatabaseManager;

class MysqlService extends DatabaseService
{
    // -- Получаем все базы на сервере
    public function getDatabaseList() : array
    {
        $sql = "SHOW DATABASES;";
        $list = $this->db->query($sql);
        return $this->dbListRender($list);
    }

    // -- Получить список таблиц
    public function getTables() : array {
        $dbname = $this->dbname;
        $sql = "SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = '$dbname'";
        $list = $this->db->query($sql);
        return $this->tableListRender($list);
    }

    // -- Получить список полей таблицы
    public function getFields(string $table) : array {
        $dbname = $this->dbname;
        $sql = "SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE `TABLE_NAME` = '$table' AND `TABLE_SCHEMA` = '$dbname'";
        $list = $this->db->query($sql);
        return $this->fieldsRender($list);
    }

}