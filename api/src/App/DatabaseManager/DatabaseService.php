<?php

namespace Dzion\Api\App\DatabaseManager;

use Dzion\Api\Interfaces\DatabaseServiceInterface;
use Dzion\Api\Interfaces\DBInterface;

class DatabaseService implements DatabaseServiceInterface
{

    protected DBInterface $db;
    protected string $dbname;
    protected string $driver;
    protected array  $params;

    public function __construct(DBInterface $db, array $params = [])
    {
       $this->setDatabase($db);
       $this->params = $params;
    }

    // -- Создаем новую базу
    public function createDatabase(string $dbName){
        $query = "CREATE DATABASE {$dbName}";
        return $this->db->execute($query);
    }

    // -- Удаляем базу
    public function dropDatabase(string $dbName){
        $query = "DROP DATABASE {$dbName}";
        return $this->db->execute($query);
    }

    public function makeSqlCommand(array $command, string $action) : array {

        $sql     = $command[$this->driver];
        $replace = $command['param'];
        $fnType  = $command['type'];

        $getArguments = $this->params;

        if(!empty($getArguments)) {
            $data = [];
            foreach ($replace as $key => $name) {
                if(isset($getArguments[$name])) {
                    $data[$key] = $getArguments[$name];
                }
            }
            if(!empty($data)) $getArguments = $data;
            $sql = str_replace($replace, $getArguments, $sql);
        }

        if($fnType == 'exec') $result = $this->db->execute($sql);
        else                  $result = $this->db->query($sql);

        if($action == 'users') $result = $this->usersRender($result);

        return $result;
    }

    public function setDatabase(DBInterface $db) : void {
        $this->db = $db;
        $this->dbname = $this->db->getDbName();
        $this->driver = $this->db->getDriver();
    }

    protected function tableListRender($tables) {
        $result = [];
        foreach ($tables as $key => $item) {
            foreach ($item as $name => $value) {
                $newName = strtolower($name);
                $result[$key][$newName] = $value;
            }
        }
        return $result;
    }

    protected function dbListRender(array $list) : array {
        foreach ($list as $key => $item) {
            $dbname = (!empty($item['Database'])) ? $item['Database'] : $item['datname'];
            $list[$key]['name'] = $dbname;
        }
        return $list;
    }

    protected function fieldsRender(array $fields) : array {
        $result = [];
        foreach ($fields as $key => $item) {
            foreach ($item as $name => $value) {
                $newName = strtolower($name);
                $result[$key][$newName] = $value;
            }
        }
        return $result;
    }

    protected function usersRender(array $list) : array {
        $field = ($this->driver == 'mysql') ? 'User' : 'usename';
        $users = [];
        foreach ($list as $key => $item) {
            $name = $item[$field];
            $item['name'] = $name;
            $users[$key] = $item;
        }
        return $users;
    }

}