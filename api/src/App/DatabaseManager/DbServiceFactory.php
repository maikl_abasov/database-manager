<?php

namespace Dzion\Api\App\DatabaseManager;

use Dzion\Api\App\DatabaseManager\MysqlService;
use Dzion\Api\App\DatabaseManager\PgsqlService;
use Dzion\Api\Interfaces\DatabaseServiceInterface;
use Dzion\Api\Interfaces\DBInterface;

class DbServiceFactory
{
    protected DatabaseServiceInterface $adapter;

    public function __construct(DBInterface $db, array $params = []) {

        $driver = $db->getDriver();

        switch ($driver) {
            case 'mysql' : $this->adapter = new MysqlService($db, $params); break;
            case 'pgsql' : $this->adapter = new PgsqlService($db, $params); break;
        }

    }

    public function getService() : DatabaseServiceInterface {
        return $this->adapter;
    }

}