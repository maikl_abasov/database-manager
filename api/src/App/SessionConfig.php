<?php

namespace Dzion\Api\App;

class SessionConfig
{
    protected static array $headers = [];

    protected static function setHeaders() {
        self::$headers = (array)getallheaders();
    }

    protected static function getHeader(string $key) : string | bool {
        return (!empty(self::$headers[$key])) ? self::$headers[$key] : false;
    }

    public static function setConnectId(string $headName = HEADER_CONF_NAME,
                                 string $sessName = SESSION_CONF_NAME) : string {
        // '35ff81045aa37e317d513883be326fc4' mysql;
        // '1af2fac71e1b2c5245dc9e69dfb09c24' pgsql
        if(empty(self::$headers)) self::setHeaders();
        $connectId = self::getHeader($headName);
        $_SESSION[$sessName] = $connectId;
        return $connectId;
    }

    public static function getConnectId(string $sessName = SESSION_CONF_NAME) : string {
        if(!empty($_SESSION[$sessName])) return $_SESSION[$sessName];
        return self::setConnectId();
    }
}