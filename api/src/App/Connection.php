<?php

namespace Dzion\Api\App;

use Dzion\Api\Core\Controller;
use Dzion\Api\Services\JsonService;

class Connection extends Controller {

    protected $filePath = CONNECTION_FILE;
    protected JsonService $json;

    public function __construct() {
        parent::__construct();
        $this->json = new JsonService($this->filePath);
    }

    public function getConnections() {
        return (array)$this->json->get();
    }

    public function getConnect($key) {
        return (array)$this->json->item($key);
    }

    public function createConnect() {
        $post = $this->post;
        $index = $this->createHashIndex($post);
        if(!$index)  return ['error' => 'Нет данных'];
        $list = $this->json->merge([$index  => $post]);
        return ['list' => $list, 'index' => $index];
    }

    public function updateConnect($key) {
        $post = $this->post;
        if(empty($post))  return ['error' => 'Нет данных'];
        $list  = $this->json->update($post, $key);
        return ['list' => $list];
    }

    public function deleteConnect($key) {
        $list = $this->json->delete($key);
        return ['list' => $list];
    }

    protected function createHashIndex($item) {
        $host = (!empty($item['host'])) ? $item['host'] : false;
        $dbname = (!empty($item['dbname'])) ? $item['dbname'] : false;
        $user = (!empty($item['user'])) ? $item['user'] : false;
        $pwd  = (!empty($item['password'])) ? $item['password'] : '';
        if(!$host || !$dbname || !$user) return false;
        return md5($host . $dbname .$user . $pwd . rand(5, 2500));
    }

}
