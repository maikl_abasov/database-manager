<?php

namespace Dzion\Api\App;

class Migration
{

    private $db;
    private $utils;

    public function __construct($database, $utils)
    {
        $this->db = $database;
        $this->utils = $utils;
    }

    public function createDatabase($database_name)
    {
        $create_database_syntex = "CREATE DATABASE %s";
        $sql = $this->db->exec(sprintf($create_database_syntex, $database_name));
        return (is_int($sql) && $sql > 0);
    }

    public function createTable($table_name, $columns)
    {
        $query = "";
        foreach ($columns as $column) {
            $query .= implode(" ", $column) . ", ";
        }

        $query = rtrim($query, ", ");
        $table_name = $this->utils->sanitize($table_name);
        $sql = sprintf("CREATE TABLE IF NOT EXISTS %s (%s);", $table_name, $query);
        $this->db->query($sql);
        return $this->db->execute();
    }

    public function setPrimary($table_name, $column_name)
    {
        $this->db->query(
            sprintf(
                "ALTER TABLE %s ADD PRIMARY KEY (%s);",
                $this->utils->sanitize($table_name),
                $this->utils->sanitize($column_name)
            )
        );
        return $this->db->execute();
    }


    public function setAutoinc($table_name, $column_array)
    {
        $this->db->query(sprintf(
            "ALTER TABLE %s MODIFY %s AUTO_INCREMENT;",
            $this->utils->sanitize($table_name),
            implode(" ", $column_array)
        ));
        return $this->db->execute();
    }


    public function setUnique($table_name, $column_name)
    {
        $this->db->query(sprintf(
            "ALTER TABLE %s ADD UNIQUE KEY %s (%s);",
            $this->utils->sanitize($table_name),
            $column_name,
            $column_name
        ));
        return $this->db->execute();
    }


    public function createColumn($table_name, $column, $after = null)
    {
        $create_column_syntex = "ALTER TABLE %s ADD %s";
        $column = implode(" ", $column);
        $sql = sprintf($create_column_syntex, $table_name, $column);
        if ($after != null) {
            $sql .= " AFTER  " . $this->utils->sanitize($after);
        }
        $sql = $sql . ";";
        $this->db->query($sql);
        return $this->db->execute();
    }

    public function updateColumnType($table_name, $column_array)
    {
        $alter_syntax = "ALTER TABLE %s MODIFY COLUMN %s;";
        $sql = sprintf($alter_syntax, $table_name, implode(" ", $column_array));
        $this->db->query($sql);
        return $this->db->execute();
    }

    public function renameTable($oldTable, $newTable)
    {
        $sql = sprintf(
            "ALTER TABLE %s RENAME TO %s;",
            $this->utils->sanitize($oldTable),
            $this->utils->sanitize($newTable)
        );

        $this->db->query($sql);
        return $this->db->execute();
    }

    public function insertValue($table_name, $columns_array)
    {
        $sql = sprintf(
            "INSERT INTO %s (%s) VALUES (%s)",
            $this->utils->sanitize($table_name),
            implode(", ", array_keys($columns_array)),
            ":" . implode(",:", array_keys($columns_array))
        );

        $this->db->query($sql);

        foreach ($columns_array as $key => $value) {
            $this->db->bind(":" . $key, $value);
        }

        return $this->db->execute();
    }

    public function updateValue($table_name, $column_name, $value)
    {
        $sql = sprintf(
            "UPDATE %s SET %s = :value",
            $this->utils->sanitize($table_name),
            $this->utils->sanitize($column_name)
        );

        $this->db->query($sql);
        $this->db->bind(":value", $value);
        return $this->db->execute();
    }

    public function dropTable($table_name)
    {
        $sql = sprintf("DROP TABLE %s;", $this->utils->sanitize($table_name));
        $this->db->query($sql);
        return $this->db->execute();
    }

    public function dropColumn($table_name, $column_name)
    {
        $sql = sprintf(
            "ALTER TABLE %s DROP COLUMN %s;",
            $this->utils->sanitize($table_name),
            $this->utils->sanitize($column_name)
        );

        $this->db->query($sql);
        return $this->db->execute();
    }

    public function dropDatabase($database_name)
    {
        $drop_db_syntax = "DROP DATABASE %s";
        $sql = sprintf($drop_db_syntax, $database_name);
        $this->db->query($sql);
        return $this->db->execute();
    }

    public function createIndex($table, $column)
    {
        $sql = sprintf("ALTER TABLE %s ADD INDEX(%s);", $table, $column);
        $this->db->query($sql);
        $this->db->execute();
    }
}
