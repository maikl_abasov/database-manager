<?php

namespace Dzion\Api\Core;

class App
{
    protected $route;

    public function __construct(string $pathInfo) {
        $this->route = Router::run($pathInfo);
        if(empty($this->route)) {
            throw new \Exception('Не найден маршрут (route)!');
        }
    }

    public function handle() {

        $controller = $this->route['controller'];
        $params = $this->route['params'];
        $response = null;

        if($controller instanceof \Closure) {
            $response = call_user_func_array($controller, array_values($params));
        }

        return $response;
    }



}