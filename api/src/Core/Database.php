<?php

namespace Dzion\Api\Core;

use PDO;
use Exception;
use PDOException;
use Dzion\Api\Interfaces\DBInterface;

class Database implements DBInterface
{
    protected PDO $pdo;
    protected array $config;

    public function __construct(array $config = [], bool $connect = true)
    {
        $this->setConfig($config);
        if($connect) $this->connect($config);
    }

    public function setConfig(array $config = []) : self
    {
        if(!empty($config)) $this->config = $config;
        return $this;
    }

    public function connect(array $config = []) : void
    {
        $this->setConfig($config);
        $port = (!empty($this->config['port'])) ? $this->config['port'] : 3306;
        $driver   = $this->config['driver'];
        $host     = $this->config['host'];
        $dbname   = $this->config['dbname'];
        $user     = $this->config['user'];
        $password = $this->config['password'];
        // $charset  = $this->config['charset'];

        try {
            $options = [
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                PDO::ATTR_EMULATE_PREPARES => false,
            ];

            $dsn = "$driver:host=$host;dbname=$dbname;port=$port;";
            $this->pdo = new PDO($dsn, $user, $password, $options);

        } catch (PDOException $e) {
            throw new Exception('DB-CONNECT-ERROR:' . $e->getMessage());
        }
    }

    private function isError() : void
    {
        $error = $this->pdo->errorInfo();
        if (!empty($error[1])) throw new Exception($error);
    }

    public function execute(string $sql) : mixed
    {
        $result = $this->pdo->exec($sql);
        $this->isError();
        return $result;
    }

    public function query(string $sql) : mixed
    {
        $stmt = $this->pdo->query($sql);
        $this->isError();
        return $stmt->fetchAll();
    }

    public function getPdo() : PDO {
        return $this->pdo;
    }

    public function getDriver() : string {
        return $this->getConfig('driver');
    }

    public function getDbName()  : string {
        return $this->getConfig('dbname');
    }

    public function getConfig(string $name = null) : mixed {
        return (!empty($this->config[$name])) ? $this->config[$name] : $this->config;
    }

    public function setDbName(string $dbname, bool $connect = false) : void
    {
        $this->config['dbname'] = $dbname;
        if($connect) $this->connect();
    }

}