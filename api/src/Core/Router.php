<?php

namespace Dzion\Api\Core;

class Router {

    private static $routes = array();

    private function __construct() {}
    private function __clone() {}

    public static function route($pattern, $callback) {
        $pattern = self::patternFormat($pattern);
        $pattern = '/^' . str_replace('/', '\/', $pattern) . '$/';
        self::$routes[$pattern] = $callback;
    }

    public static function run(string $pathInfo) : array {
        foreach (self::$routes as $pattern => $callback) {
            if (preg_match($pattern, $pathInfo, $params)) {
                array_shift($params);
                return [
                    'controller' => $callback,
                    'params' => $params
                ];
            }
        }
        return [];
    }

    private static function patternFormat($pattern) {
        $list = explode('/', $pattern);
        foreach ($list as $key => $value) {
            if(isset($value[0]) && $value[0] == "{") {
                $list[$key] = "(\w+)";
            }
        }
        return implode("/", $list);
    }

}