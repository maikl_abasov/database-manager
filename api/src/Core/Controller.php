<?php

namespace Dzion\Api\Core;

class Controller
{
    protected $post    = [];
    protected $get     = [];
    protected $request = [];
    protected $server  = [];

    public function __construct() {
        $this->initRequest();
    }

    public function initRequest() {
        $this->get     = $_GET;
        $this->post    = (!empty($_POST)) ? $_POST : [];
        $this->request = $_REQUEST;
        $this->server  = $_SERVER;
    }

    public function post(string $name) : string {
        return (!empty($this->post[$name])) ? $this->post[$name] : '';
    }

    public function get(string $name) : string {
        return (!empty($this->server[$name])) ? $this->server[$name] : '';
    }

    public function server(string $name) : string {
        return (!empty($this->post[$name])) ? $this->post[$name] : '';
    }

    public function request(string $name) : string {
        return (!empty($this->request[$name])) ? $this->request[$name] : '';
    }

}