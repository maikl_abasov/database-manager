<?php

namespace Dzion\App\Facade;

use Dzion\App\Container\Container;

class Facade
{
    public static function __callStatic($method, $params = [])
    {
        $instance = static::getFacadeRoot();

        if (!$instance) {
            throw new RuntimeException('A facade root has not been set.');
        }

        return $instance->$method(...$params);
    }

    public static function getFacadeRoot()
    {
        return static::resolveFacadeInstance(static::getFacadeAccessor());
    }

    protected static function resolveFacadeInstance($object)
    {
        if (is_object($object)) return $object;

        return (Container::getInstance())->get($object);
    }

}