<?php

namespace Dzion\Api\Core\Container;

use ReflectionClass;
use ReflectionMethod;
use ReflectionParameter;

class Container implements ContainerInterface
{

    protected array $container = [];
    public static $instance = null;

    public static function getInstance() : self
    {
       if(!self::$instance) {
           return self::$instance = new self();
       }
       return self::$instance;
    }

    public function get(string $key, mixed $params = null, bool $static = true) : mixed
    {

        if(!empty($params)) $this->setParams($params);

        if($this->has($key)) {
            $object = $this->container[$key];
            if($object instanceof \Closure) {
                $object = $object($this, $params);
                if($static) $this->container[$key] = $object;
            }
        } else {
            $object = $this->resolveClass($key);
        }

        return $object;
    }

    public function has(string $key) : bool
    {
        return (isset($this->container[$key])) ? true : false;
    }

    public function set(string $key, mixed $object) : void
    {
        $this->container[$key] = $object;
    }

    public function setParams(array $params) : void
    {
        foreach ($params as $name => $item) {
            $this->container[$name] = $item;
        }
    }

    public function callMethod(string $class, string $method, array $params = null) : mixed
    {
        if(!empty($params)) $this->setParams($params);

        $reflectClass = $this->resolveClass($class);

        $parameters = $this->getMethodParams($class, $method);

        if(empty($parameters)) {
            return $reflectClass->$method();
        }

        $dependencies = $this->getDependencies($parameters);

        return $reflectClass->$method(...$dependencies);
    }

    protected function resolveClass(string $class) : mixed
    {
        $reflectionClass = new ReflectionClass($class);

        if (!$reflectionClass->hasMethod('__construct')) return new $class;

        $parameters = $this->getMethodParams($class);

        $dependencies = $this->getDependencies($parameters);

        return new $class(...$dependencies);
    }

    protected function getMethodParams(string $class, string $method = '__construct') : array
    {
        $reflectionMethod = new ReflectionMethod($class, $method);
        return $reflectionMethod->getParameters();
    }

    protected function getDependencies(array $parameters)
    {
        $dependencies = [];

        foreach ($parameters as $reflectionParam) {

            $paramType = $reflectionParam->getType()->getName();
            $paramName = $reflectionParam->getName();

            if($this->has($paramType)) {
                $dependencies[] = $this->get($paramType);
            } elseif(class_exists($paramType)) {
                $dependencies[] = $this->get($paramType);
            } elseif($this->has($paramName)) {
                $dependencies[] = $this->get($paramName);
            } elseif(class_exists($paramName)) {
                $dependencies[] = $this->get($paramName);
            } else {

            }
        }

        return $dependencies;
    }

}
