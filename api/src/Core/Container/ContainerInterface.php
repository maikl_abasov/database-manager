<?php

namespace Dzion\Api\Core\Container;

interface ContainerInterface
{
    public function get(string $key, mixed $params = null) : mixed;
    public function has(string $key) : bool;
}