<?php

namespace Dzion\Api\Services;

use Dzion\Api\Interfaces\FileSaveInterface;

class FileSaveService implements FileSaveInterface
{

    public function getJsonFile($filePath)
    {
        $data = [];
        if (file_exists($filePath))
            $data = (array)json_decode(file_get_contents($filePath), true);
        return $data;
    }


    public function saveJson($filePath, $newData)
    {
        if (file_exists($filePath)) {
            $jsonData = (array)json_decode(file_get_contents($filePath), true);
            $jsonData = array_merge($jsonData, $newData);
        } else {
            $jsonData = $newData;
        }

        file_put_contents($filePath, json_encode($jsonData, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
    }


    public function saveTxt($filePath, $data, $delimiter = ";")
    {
        foreach ($data as $key => $item) {
            $data[$key] = implode($delimiter, $item) . "\n";
        }
        file_put_contents($filePath, implode("", $data), FILE_APPEND | LOCK_EX);
    }


    public function saveCsv($filePath, $data, $delimiter = ";")
    {
        $fp = fopen($filePath, 'a');
        foreach ($data as $item) {
            fputcsv($fp, $item, $delimiter, '"');
        }
        fclose($fp);
    }
}