<?php

namespace Dzion\Api\Services;

use Josantonius\Json\Json;

class JsonService extends Json
{
    public function delete(string $key): mixed
    {
        $data = $this->get();
        unset($data[$key]);
        return $this->saveToJsonFile($data);
    }

    public function update(array $item, string $key): mixed
    {
        $data = $this->get();
        $data[$key] = $item;
        return $this->saveToJsonFile($data);
    }

    public function item(string $key): mixed
    {
        $data = $this->get();
        return $data[$key];
    }
}