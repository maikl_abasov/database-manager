<?php

namespace Dzion\Api\Services;

class MemoryUsageInfo
{

    private $real_usage;
    private $time = [];

    private array $statistics = [];

    public function __construct($real_usage = false) {
         $this->real_usage = $real_usage;
    }

    public function getCurrentMemoryUsage($with_style = true) {
        $mem = memory_get_usage($this->real_usage);
        return ($with_style) ? $this->byteFormat($mem) : $mem;
    }

    public function getPeakMemoryUsage($with_style = true) {
        $mem = memory_get_peak_usage($this->real_usage);
        return ($with_style) ? $this->byteFormat($mem) : $mem;
    }

    public function setMemoryUsage($info = '') {
        $this->statistics[] = [
            'time' => date('H:i:s'),
            'timestamp' => time(),
            'info' => $info,
            'memory_usage' => $this->getCurrentMemoryUsage()
        ];
    }

    public function printMemoryUsageInfo() {
        $info = ['Start Time: ' . $this->time['start']];
        foreach ($this->statistics as $satistic) {
            $info[] = " Timestamp: " . $satistic['timestamp'] .
                      " | Time: "    . $satistic['time'] .
                      " | Memory Usage: " . $satistic['memory_usage'] .
                      " | Info: " . $satistic['info'];
        }
        $info[] = 'End Time: ' . $this->time['end'];
        $info[] = "Пик использования памяти : " . $this->getPeakMemoryUsage();
        $info[] = 'Финал: ' .$this->getCurrentMemoryUsage(true);
        return $info;
    }

    public function setStart($info = 'Initial Memory Usage') {
        if(empty($this->time['start'])) $this->time['start'] = date('H:i:s');
        $this->setMemoryUsage($info);
    }

    public function setEnd($info = 'Memory Usage at the End') {
        if(empty($this->time['end']))  $this->time['end'] = date('H:i:s');
        $this->setMemoryUsage($info);
    }

    private function byteFormat($bytes, $unit = "", $decimals = 2) {
        $units = ['B' => 0, 'KB' => 1,
                  'MB' => 2, 'GB' => 3,
                  'TB' => 4, 'PB' => 5,
                  'EB' => 6, 'ZB' => 7,
                  'YB' => 8 ];

        $value = 0;
        if ($bytes > 0) {
            if (!array_key_exists($unit, $units)) {
                $pow = floor(log($bytes)/log(1024));
                $unit = array_search($pow, $units);
            }

            $value = ($bytes/pow(1024,floor($units[$unit])));
        }

        if (!is_numeric($decimals) || $decimals < 0) {
            $decimals = 2;
        }

        return sprintf('%.' . $decimals . 'f '.$unit, $value);
    }

}