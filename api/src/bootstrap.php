<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

//define('CONFIG_ID_NAME', 'X-CONNECT-ID'); // header connect_id (получаем от фронта)
define('HEADER_CONF_NAME', 'X-CONNECT-ID'); // header connect_id (получаем от фронта)
define('SESSION_CONF_NAME', 'CONNECT_ID'); // session connect_id (получаем в app)

define('DS', DIRECTORY_SEPARATOR);
define('CLASS_DIR', ROOT_DIR . DS . 'src');
define('CONFIG_DIR', ROOT_DIR . DS . 'config');
define('CONNECTION_FILE', ROOT_DIR . '/storage/connections.json');
define('USERS_FILE', ROOT_DIR . '/storage/users.json');
define('MIGRATIONS_DIR', ROOT_DIR . '/storage/migrations');
define('MIGRATION_FILES', MIGRATIONS_DIR . '/files');
define('LOGS_DIR', ROOT_DIR . '/logs');

//$SQL_COMMANDS = include CONFIG_DIR .  '/sql_commands.php';
// include CLASS_DIR . '/Autoloader.php';

require_once CONFIG_DIR . '/helpers.php';

set_error_handler('custom_fatal_error_hanler');
set_exception_handler('error_exception_handler');

require_once CONFIG_DIR . '/routes.php';
require_once CONFIG_DIR . '/app.php'; // container
