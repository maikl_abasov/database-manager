<?php

namespace Dzion\Api\Migrate;

use Dzion\Api\Interfaces\DBInterface;
use Dzion\Api\Interfaces\FileSaveInterface;
use PDO;
use SplFileInfo;

class DbMigrateBase
{

//    protected PDO $pdo;
//    protected string $rootMigrationFiles;
//    protected string $migrateFileDir;
//    protected string $driver;
//    protected string $dbname;
//    public string $schema = 'public';
//    protected string $delimiter = ";";
//    protected int $exportLimit  = 85000;
//    protected int $importLimit  = 150;

    //protected string $rootMigrationFiles;
    //protected string $migrateFileDir;

    public string $schema;
    protected array $config;
    protected string $driver;
    protected string $dbname;

    protected string $migrateFileDir;
    protected string $rootMigrationFiles;
    protected string $rootMigrationDir;

    protected DBInterface $db;

    public function __construct(DBInterface $database, string $rootMigrationDir, string $schema = 'public')
    {
        $this->db = $database;
        $this->rootMigrationDir = $rootMigrationDir;
        $this->schema = $schema;
        $this->init();
    }

    protected function init()
    {
        $this->rootMigrationFiles = $this->rootMigrationDir . '/files';
        $this->config = $this->db->getConfig();
        $this->driver = $this->config['driver'];
        $this->dbname = $this->config['dbname'];

    }

    public function setMigrateDir() : void
    {
        $migrateDir = $this->rootMigrationFiles . '/' . $this->config['host'] . '__' . $this->dbname;
        if (file_exists($migrateDir)) {
            $this->recursiveRemoveDir($migrateDir);
        }

        @mkdir($migrateDir, 0777, true);
        @mkdir($migrateDir . '/data', 0777, true);
        @mkdir($migrateDir . '/tables', 0777, true);

        $this->migrateFileDir = $migrateDir;
    }

    protected function recursiveRemoveDir($dir) : void
    {
        $files = new \FilesystemIterator($dir);
        foreach ($files as $file) {
            if (is_dir($file) && !is_link($file)) {
                $this->recursiveRemoveDir($file);
            } else {
                unlink($file);
            }
        }
        rmdir($dir);
    }

    // -- Получить список таблиц
    public function getTables(): array
    {
        $data = [
            'pgsql' => "SELECT * FROM information_schema.tables WHERE table_schema = '" .$this->schema. "' AND table_catalog = '" .$this->dbname. "'",
            'mysql' => "SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = '" .$this->dbname. "' AND TABLE_TYPE = 'BASE TABLE'",
        ];
        $sql = $data[$this->driver];
        $tables = $this->db->query($sql);
        return $this->toLowerCase($tables);
    }

    // -- Получить список полей таблицы
    public function getFields(string $table): array
    {
        $data = [
            'mysql' => "SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE `TABLE_NAME` = '{$table}' AND `TABLE_SCHEMA` = '" .$this->dbname. "'",
            'pgsql' => "SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '{$table}'",
        ];
        $sql = $data[$this->driver];
        $fields = $this->db->query($sql);
        return $this->toLowerCase($fields);
    }

    // -- Получить список таблиц с набором полей
    public function setFields(array $tableList): array
    {
        $tables = [];
        foreach ($tableList as $item) {
            $tableName = $item['table_name'];
            $fields = $this->getFields($tableName);
            $tables[$tableName] = $fields;
        }
        return $tables;
    }

    protected function toLowerCase(array $list) : array {
        $result = [];
        foreach ($list as $key => $item)
            foreach ($item as $name => $value)
                $result[$key][strtolower($name)] = $value;
        return $result;
    }

    public function scanDir($dirPath, $render = false)
    {
        $files = glob($dirPath);
        if($render) $files = $this->filesRender($files);
        return $files;
    }

    public function filesRender($files)
    {
        foreach ($files as $key => $filePath) {
            $name = $this->getFileName($filePath);
            $count = (is_file($filePath)) ? count(file($filePath)) : '';
            $files[$key] = [
                'path' => $filePath,
                'name' => $name,
                'count' => $count
            ];
        }
        return $files;
    }

    public function getFileName($filePath)
    {
        $fileInfo = new SplFileInfo($filePath);
        $name = $fileInfo->getFilename();
        return $name;
    }














//    protected function connect(): void
//    {
//
//        $config = $this->config;
//
//        $host = $config['host'];
//        $port = $config['port'];
//        $dbname = $config['dbname'];
//        $driver = $config['driver'];
//        $user = $config['user'];
//        $password = $config['password'];
//
//        $options = [
//            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
//            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
//        ];
//
//        try {
//            $dsn = "$driver:host=$host;dbname=$dbname;port=$port";
//            $this->pdo = new PDO($dsn, $user, $password, $options);
//            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
//        } catch (Exception $err) {
//            $this->error($err, ['pdo' => 'PDO INIT ERROR']);
//        }
//    }



//    // -- Проходим циклом по всем таблицам
//    public function getFields(array $tables)
//    {
//        $result = [];
//        $tableName = ($this->driver == 'pgsql') ? 'table_name' : 'TABLE_NAME';
//        foreach ($tables as $key => $table) {
//            $name = $table[$tableName];
//            $fields = $this->getTableFields($name);
//            $result[$name] = $fields;
//        }
//        return $result;
//    }

    // --


//    public function select($sql)
//    {
//        return $this->make($sql, 'select');
//    }
//
//    public function exec($sql)
//    {
//        return $this->make($sql, 'exec');
//    }
//
//    public function query($sql)
//    {
//        return $this->make($sql, 'query');
//    }
//
//    public function make($sql, $type = 'select')
//    {
//        try {
//            switch ($type) {
//                case 'exec'  :
//                    $result = $this->pdo->exec($sql);
//                    break;
//                case 'query' :
//                    $result = $this->pdo->query($sql);
//                    break;
//                default  :
//                    $sth = $this->pdo->prepare($sql);
//                    $sth->execute();
//                    $result = $sth->fetchAll();
//                    break;
//            }
//        } catch (\Exception $err) {
//            $error = ['pdo' => 'PDO MAKE ERROR', 'sql' => $sql, 'type' => $type];
//            $this->error($err, $error);
//        }
//        return $result;
//    }
//
//    protected function error($err, $error = [])
//    {
//        $error['message'] = $err->getMessage();
//        $error['code'] = $err->getCode();
//        $out = print_r(['error' => $error], true);
//        die("<pre>$out</pre>");
//    }
//
//    protected function getMemory($memory = false)
//    {
//        return $memory === false ? memory_get_usage() : memory_get_usage() - $memory;;
//    }
//
//    protected function getTime($time = false)
//    {
//        return $time === false ? microtime(true) : microtime(true) - $time;
//    }

}
