<?php

namespace Dzion\Api\Migrate;

use Dzion\Api\Interfaces\DBInterface;
use Dzion\Api\Interfaces\FileSaveInterface;
use Dzion\Api\Services\MemoryUsageInfo;

class FileExporter extends DbMigrateBase
{
    protected $limit = 85000;
    protected FileSaveInterface $fileSaver;

    public function __construct(DBInterface $database, string $rootMigrationDir, FileSaveInterface $fileSaver, string $schema = 'public')
    {
        parent::__construct($database, $rootMigrationDir, $schema);
        $this->fileSaver = $fileSaver;
    }

    public function run()
    {
        $startDate = date('H:i:s');
        $this->setMigrateDir(); // устанавливаем директорию

        $tableList = $this->getTables();  // получаем таблицы
        $tables = $this->setFields($tableList); // получаем таблицы c набором полей

        $respTables = $this->exportTables($tables, 'json');
        $respData = $this->exportTablesData($tables, 'json');

        $time = ['start' => $startDate, 'end' => date('H:i:s'), 'memory' => []];

        return [
           'time'   => $time,
           'tables' => $respTables,
           'data'   => $respData,
        ];
    }

    public function exportTables($tables, $ext = 'json')
    {
        $response = [];
        $path = $this->migrateFileDir . '/tables/';
        foreach ($tables as $tableName => $fields) {
            $tableFile = $path . $tableName . '.' . $ext;
            $this->fileSaver->saveJson($tableFile, $fields);
            $response[$tableName] = $fields;
        }
        return $response;
    }

    public function exportTablesData($tables, $ext = 'csv')
    {
        $response = [];
        $pdo = $this->db->getPdo();
        $path = $this->migrateFileDir . '/data/';
        foreach ($tables as $tableName => $fields) {
            $resp = $pdo->query("SELECT COUNT(*) FROM {$tableName}");
            $allItemsCount = $resp->fetchColumn();
            $tableFile = $path . $tableName . '.' . $ext;
            $response[$tableName] = $this->exportData($tableName, $tableFile, $allItemsCount);
        }
        return $response;
    }

    protected function exportData(string $tableName, string $migrateFile, int $allItemsCount): array
    {
        $page  = 1;
        $limit = $this->limit;
        if(!$allItemsCount) return ['page' => $page, 'count' => $allItemsCount];

        if($allItemsCount <= $limit) {
            $data = $this->db->query("SELECT * FROM {$tableName}");
            $this->fileSaver->saveCsv($migrateFile, $data);
        } else {
            while (true) {
                // --- получаем данные из базы ---
                $start = ($page - 1) * $limit;
                $data = $this->db->query("SELECT * FROM {$tableName} LIMIT $start, $limit");
                // --- условие выхода из цикла ---
                if (empty($data)) break;
                $this->fileSaver->saveCsv($migrateFile, $data);
                $page++;
            }
        }

        return ['page' => $page, 'count' => $allItemsCount];
    }

}