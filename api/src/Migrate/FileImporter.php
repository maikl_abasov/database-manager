<?php

namespace Dzion\Api\Migrate;

use SplFileInfo;
use Exception;
use PDOException;
use PDO;

class FileImporter extends DbMigrateBase
{

    protected string $delimiter = ";";

    protected int $importLimit  = 4; // 150

    public function run($migrateFile)
    {
        $time = ['start' => date('H:i:s')];

        $this->migrateFileDir = $this->rootMigrationFiles . '/' . $migrateFile; // устанавливаем директорию из которой будем читать
        $tableFiles = $this->scanDir($this->migrateFileDir . '/tables/*');  // получаем файлы таблиц
        $dataFiles  = $this->scanDir($this->migrateFileDir . '/data/*');  // получаем файлы таблиц

        $tables = $this->importTables($tableFiles);
        $data   = $this->dataFilesImport($dataFiles, $tables);

        $time['end'] = date('H:i:s');

        return [
            'time' => $time,
            //'tables' => $tables,
            //'data' => $data,
        ];
    }

    public function importTables(array $tableFiles)
    {
        $tables = [];
        foreach ($tableFiles as $tableFile) {
            $fields = (array)json_decode(file_get_contents($tableFile), true);
            $tableName = $this->getTableName($tableFile);
            $tables[$tableName] = $this->createTable($tableName, $fields);
            $tables[$tableName]['fields'] = $fields;
        }
        return $tables;
    }

    protected function getTableName(string $tableFile) : string
    {
        $fileInfo = new SplFileInfo($tableFile);
        $tableName = explode('.', $fileInfo->getFilename())[0];
        return $tableName;
    }

    protected function setType(string $type) : string
    {
        $_type = $this->resolveType($type);
        if(!$_type) $_type = " VARCHAR ";
        if ($_type == 'varchar' || $_type == 'VARCHAR') $_type = $_type . '(350)';
        return $_type;
    }

    protected function setPrimaryKey(array $item): array | bool
    {
        $sql = $name = false;
        if ((!empty($item['extra'])) && ($item['extra'] == 'auto_increment')) {
            $name = $item['column_name'];
        } elseif(!empty($item['column_key'] && $item['column_key'] == 'PRI')) {
            $name = $item['column_name'];
        } elseif(!empty($item['column_default'])) {
            if (str_contains($item['column_default'], 'nextval')) {
                $name = $item['column_name'];
            }
        }

        if (!$name) return $sql;

        $sql =  ($this->driver == 'mysql') ? "{$name} int PRIMARY KEY AUTO_INCREMENT"
            : "{$name} SERIAL PRIMARY KEY";

        return ['name' => $name, 'sql'  => $sql];
    }

    protected function resolveType(string $type) : string
    {
        $types = [
            'integer' => 'int',
            'character varying' => 'varchar',
            'timestamp without time zone' => 'varchar',
            'smallint'  => 'int',
            'tinytext'  => 'text',
            'tinyint'   => 'int',
            'char'      => 'varchar',
            'bigint'    => 'bigint',
            'longtext'  => 'text',
            'enum'      => 'varchar',
            'double'    => 'varchar',
            'datetime'  => 'varchar',
            'timestamp' => 'varchar',
            'TIMESTAMP' => 'varchar',
            'varbinary' => 'varchar'
        ];

        return (isset($types[$type])) ? $types[$type] : $type;
    }

    public function createTable(string $tableName, array $fields)
    {
        $addFields = $list = [];
        $wrap = ($this->driver == 'pgsql') ? '"' : '`';

        foreach ($fields as $item) {

            if (!empty($primary = $this->setPrimaryKey($item))) {
                $addFields[] = $primary['sql'];
                $list[] = ['name' => $item['column_name'], 'type' => 'int', 'auto' => true];
                continue;
            }

            $name = $item['column_name'];
            $type = $item['data_type'];
            $type = $this->setType($type);
            $addFields[] = "{$wrap}{$name}{$wrap}  {$type} DEFAULT NULL ";

            $list[] = ['name' => $name, 'type' => $type, 'auto' => false];
        }

        $tableFields = implode(", \n", $addFields);
        $query  = " DROP TABLE IF EXISTS {$tableName}; ";
        $query .= " CREATE TABLE {$tableName} ({$tableFields}); ";
        $status = $this->db->execute($query);

        return ['status' => $status, 'insert_fields' => $list];
    }

    public function importTablesData(array $dataFiles, array $tables) : array
    {
        $response = [];
        $limit = $this->importLimit;
        $delimiter = $this->delimiter;

        foreach ($dataFiles as $tableFile) {
            $row = 0;
            $tableName = $this->getTableName($tableFile);
            $fields = $tables[$tableName]['insert_fields'];

            $handle = fopen($tableFile, "r");

            if ($handle !== FALSE) {
                $data = $resp = [];
                while (($item = fgetcsv($handle, 0, $delimiter)) !== FALSE) {
                    $row++;
                    $data[] = $item;
                    if($row > $limit) {
                        $resp[] = $this->insertData($tableName, $data, $fields);
                        $data = [];
                        $row  = 0;
                    }
                }

                if(!empty($data)) {
                    $resp[] = $this->insertData($tableName, $data, $fields);
                }
            }

            fclose($handle);

            $response[$tableName] = $resp;
        }

        return $response;
    }

    protected function dataPreparation(array $data, array $fields): array
    {

        $wrap = ($this->driver == 'pgsql') ? '"' : '`';
        $resultData = $columnValues = [];
        $primaryKey = false;
        $counter = 0;

        foreach ($data as $item) {

            $bindData = $values = $columns = [];
            foreach ($fields as $fieldKey => $field) {

                $auto = $field['auto'];
                $name = $field['name'];
                $type = $field['type'];
                if ((!empty($auto))) {
                    $primaryKey = $name;
                    continue;
                }

                $counter++;
                $bindName = ':' . $counter;

                $value = trim($item[$fieldKey]);
                switch ($type) {
                    case 'int' :
                    case 'integer' :
                        $pdoType = PDO::PARAM_INT;
                        if (!$value) $value = NULL;
                        else $value = intval($value);
                        break;

                    default :
                        if (!$value) $value = NULL;
                        $pdoType = PDO::PARAM_STR;
                        break;
                }

                $bindData[$bindName] = [
                    'value' => $value,
                    'type'  => $pdoType,
                ];

                $values[] = $bindName;
                $columns[] = $wrap . $name . $wrap;
            }

            $resultData[] = $bindData;
            $columnValues[] = '(' . implode(',', $values) . ') ' . PHP_EOL;
        }

        return [
            'columns' => $columns,
            'values'  => $columnValues,
            'data'    => $resultData,
            'primary_key' => $primaryKey,
        ];
    }

    public function insertData(string $tableName, array $data, array $fields): array
    {

        $response = $message = null;

        $dataPrepare = $this->dataPreparation($data, $fields);

        $columns = implode(',', $dataPrepare['columns']);
        $values  = PHP_EOL . implode(',', $dataPrepare['values']);
        $query   = 'INSERT INTO ' . $tableName . ' (' . $columns . ') VALUES ' . $values;

        $pdo = $this->db->getPdo();
        $pdo->beginTransaction();
        $stmt = $pdo->prepare($query);

        try {
            $bindingData = $dataPrepare['data'];
            foreach ($bindingData as $item) {
                foreach ($item as $name => $values) {
                    $stmt->bindParam($name, $values['value'], $values['type']);
                }
            }
            $response = $stmt->execute();
            $pdo->commit();
        } catch (PDOException $e) {
            $pdo->rollback();
            $message = $e->getMessage();
            $errorInfo = $stmt->errorInfo();
            // lg($stmt->rowCount());
            // $stmt->debugDumpParams();
            lg(['message' => $message, 'query' => $query, 'error_info' => $errorInfo, $bindingData]);
        }

        return [
            'query' => $query,
            'error' => $message,
            'result' => $response,
        ];
    }

    public function dataFilesImport(array $dataFiles, array $tables): array
    {
        $response = [];
        foreach ($dataFiles as $tableFile) {
            $tableName = $this->getTableName($tableFile);
            $fields = $tables[$tableName]['insert_fields'];
            $iterator = $this->readCsv($tableFile);
            foreach ($iterator as $data) {
                $response[] = $this->insertData($tableName, $data, $fields);
            }
        }
        return $response;
    }

    protected function readCsv(string $tableFile)
    {
        $row = 1;
        $handle = fopen($tableFile, "r");
        if ($handle !== FALSE) {
            $data = [];
            while (($item = fgetcsv($handle, 0, ";")) !== FALSE) {
                $row++;
                $data[] = $item;

                if ($row > 5) {
                    $row = 1;
                    $response = $data;
                    $data = [];
                    yield $response;
                }
            }

            if (!empty($data)) {
                yield $data;
            }
        }
        fclose($handle);
    }

//    public function insertData1(string $tableName, array $columnData, array $fields) : array
//    {
//
//        $wrap = ($this->driver == 'pgsql') ? '"' : '`';
//
//        $result = null;
//        $message = '';
//        $columnFields = [];
//        $autoIncKey = false;
//
//        foreach ($fields as $key => $item) {
//            $name = $item['name'];
//            if((!empty($item['auto']))) {
//                $autoIncKey =  $key;
//                continue;
//            }
//            $columnFields[$key] = $wrap . $name . $wrap;
//        }
//
//        $valuesData = $bindingData = [];
//        $counter = 0;
//        foreach ($columnData as $colKey => $item) {
//            $valueItem = $bindItem = [];
//            foreach ($item as $itemKey => $value) {
//                if($autoIncKey === $itemKey) continue;
//
//                $counter++;
//                $name = ':' . $counter;
//
//                $value = trim($value);
//                if(!$value) $value = NULL;
//                elseif(is_numeric($value)) $value = intval($value);
//
//                $bindItem[$name] = [
//                    'value' => $value,
//                    'type' => gettype($value),
//                ];
//
//                $valueItem[$name] = $name;
//            }
//
//            if(!empty($bindItem)) {
//                $valuesData[] = '(' . implode(',', $valueItem) . ') ' . PHP_EOL;
//                $bindingData[] = $bindItem;
//            }
//        }
//
//        // lg($bindingData);
//
//        $columnString = implode(',', $columnFields);
//        $valuesString = implode(',', $valuesData);
//        $query = 'INSERT INTO ' . $tableName . ' (' . $columnString . ')
//                  VALUES ' . PHP_EOL . $valuesString;
//
//        $pdo = $this->db->getPdo();
//        $pdo->beginTransaction();
//        $stmt = $pdo->prepare($query);
//
//        try {
//
//            foreach ($bindingData as $item) {
//                foreach ($item as $name => $value) {
//                    if($value['type'] == 'integer') {
//                        $stmt->bindParam($name, $value['value'], PDO::PARAM_INT);
//                    } elseif($value['type'] == 'NULL') {
//                        $stmt->bindParam($name, $value['value'], PDO::PARAM_NULL);
//                    } else {
//                        $stmt->bindParam($name, $value['value'], PDO::PARAM_STR);
//                    }
//                }
//            }
//
//            $result = $stmt->execute();
//            $pdo->commit();
//
//        } catch (PDOException $e) {
//            $pdo->rollback();
//            $message = $e->getMessage();
//            $errorInfo   = $stmt->errorInfo();
//            // lg($stmt->rowCount());
//            // $stmt->debugDumpParams();
//            lg(['message' => $message, 'query' => $query, 'error_info' => $errorInfo, $bindingData]);
//        }
//
//        return [
//            'query'  => $query,
//            'error'  => $message,
//            'result' => $result,
//        ];
//    }
//
//    protected function insertData2(string $tableName, array $columnData, array $fields) : array
//    {
//
//        $wrap = ($this->driver == 'pgsql') ? '"' : '`';
//        $result = $message = null;
//        $autoIncKey = false;
//        $columnFields = $bindingData = [];
//        $pdo = $this->db->getPdo();
//
//        foreach ($fields as $key => $item) {
//            $name = $item['name'];
//            if((!empty($item['auto']))) {
//                $autoIncKey =  $key;
//                continue;
//            }
//            $columnFields[$key] = $wrap . $name . $wrap;
//        }
//
//        foreach ($columnData as $colKey => $item) {
//
//            $bindItem = [];
//
//            foreach ($item as $itemKey => $value) {
//                if($autoIncKey === $itemKey) continue;
//                $value = trim($value);
//                $isInt = is_numeric($value);
//                if($isInt) $bindItem[$itemKey] = $value;
//                elseif(!$value) $bindItem[$itemKey] = 'NULL';
//                else $bindItem[$itemKey] = $pdo->quote($value);
//            }
//
//            if(!empty($bindItem)) {
//                $bindingData[] = '(' . implode(',', $bindItem) . ') ' . PHP_EOL;
//            }
//        }
//
//        $columnString = implode(',', $columnFields);
//        $valuesString = implode(',', $bindingData);
//        $query = 'INSERT INTO ' . $tableName . ' (' . $columnString . ')
//                  VALUES ' . PHP_EOL . $valuesString;
//
//        $pdo->beginTransaction();
//        $stmt = $pdo->prepare($query);
//
//        try {
//
//            $result = $stmt->execute();
//            $pdo->commit();
//
//        } catch (PDOException $e) {
//            $pdo->rollback();
//            $message = $e->getMessage();
//            $errorInfo   = $stmt->errorInfo();
//            // $stmt->debugDumpParams();
//            lg(['message' => $message, 'query' => $query, 'error_info' => $errorInfo, $bindingData]);
//        }
//
//        return [
//            'query'  => $query,
//            'error'  => $message,
//            'result' => $result,
//        ];
//    }

}
